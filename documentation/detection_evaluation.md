### Built-in MAPA entity detection evaluation

During the MAPA project, an exhaustive external evaluation of some of the generated models
is going to be carried out.

However, in order to quickly evaluate a trained detection model, and provided you have test-data
in the proper format (json-lines with tokens/level1_tags/level2_tags sequences), you can use
the following functionality.

It basically loads the provided test-data and the model to evaluate, and performs inference.
Then, it compares the predicted labels with the gold-labels, and calculate some metrics.

Once you have installed this repository in your current Python virtual environment using the
setup.py file, the command to run the evaluation is *mapa_eval* 
and its description is the following:

```bash
usage: MAPA detection evaluation [-h] --model_path MODEL_PATH --test_set_path
                                 TEST_SET_PATH [--cuda_device CUDA_DEVICE]
                                 [--batch_size BATCH_SIZE]

optional arguments:
  -h, --help            show this help message and exit
  --model_path MODEL_PATH
                        Path to the trained MAPA detection model to be used
  --test_set_path TEST_SET_PATH
                        Path to the test_set file (NOTE: it refers to the
                        already converted test_set to a single file)
  --cuda_device CUDA_DEVICE
                        Number of CUDA device to use (-1 means CPU)
  --batch_size BATCH_SIZE
                        The batch size during inference for evaluation
                        (defaults to 8)
```

So, for example, if you run:

```bash
mapa_eval --model_path PATH/TO/MODEL --test_set_path PATH/TO/TESTSET --cuda_device 0
```

You will evaluate the designated model towards the selected test data, using the GPU 0 to speed up
the inference (provided you machine and installation handles CUDA properly, otherwise you will
end up using CPU, but unless your test data is really large, it should not take long anyway).

The progress of the test-data process should appear in the console, and if no problem happens,
you should see a report like the following at the end:

```bash
-------------------
MAPA LEVEL 1 TAGS TOKEN-WISE evaluation:
-------------------
miP: 0.857   miR: 0.857   miF: 0.857
-------------------
MAPA LEVEL 1 TAGS CoNLL-WISE evaluation:
-------------------
processed 14954 tokens with 304 phrases; found: 388 phrases; correct: 248.
accuracy:  85.71%; (non-O)
accuracy:  97.86%; precision:  63.92%; recall:  81.58%; FB1:  71.68
          ADDRESS: precision:  75.00%; recall:  80.49%; FB1:  77.65  88
           AMOUNT: precision:  42.86%; recall:  60.00%; FB1:  50.00  28
             DATE: precision:  90.00%; recall:  91.30%; FB1:  90.65  140
     ORGANISATION: precision:   8.11%; recall:  50.00%; FB1:  13.95  74
           PERSON: precision:  64.29%; recall:  72.00%; FB1:  67.92  56
             TIME: precision: 100.00%; recall: 100.00%; FB1: 100.00  2
-------------------
MAPA LEVEL 2 TAGS TOKEN-WISE evaluation:
-------------------
miP: 0.922   miR: 0.780   miF: 0.845
-------------------
MAPA LEVEL 2 TAGS CoNLL-WISE evaluation:
-------------------
processed 14954 tokens with 562 phrases; found: 556 phrases; correct: 484.
accuracy:  77.96%; (non-O)
accuracy:  98.77%; precision:  87.05%; recall:  86.12%; FB1:  86.58
      NATIONALITY: precision:   0.00%; recall:   0.00%; FB1:   0.00  0
             ROLE: precision:   0.00%; recall:   0.00%; FB1:   0.00  2
             city: precision:  64.00%; recall: 100.00%; FB1:  78.05  50
          country: precision:  10.00%; recall: 100.00%; FB1:  18.18  20
              day: precision: 100.00%; recall: 100.00%; FB1: 100.00  122
      family name: precision:  86.36%; recall:  90.48%; FB1:  88.37  44
given name - female: precision:  90.00%; recall:  81.82%; FB1:  85.71  20
given name - male: precision: 100.00%; recall: 100.00%; FB1: 100.00  20
            month: precision: 100.00%; recall:  98.31%; FB1:  99.15  116
         other:id: precision:   0.00%; recall:   0.00%; FB1:   0.00  0
            place: precision:   0.00%; recall:   0.00%; FB1:   0.00  0
standard abbreviation: precision:   0.00%; recall:   0.00%; FB1:   0.00  2
           street: precision:   0.00%; recall:   0.00%; FB1:   0.00  4
        territory: precision: 100.00%; recall:  29.17%; FB1:  45.16  14
            title: precision:   0.00%; recall:   0.00%; FB1:   0.00  2
             unit: precision:  85.71%; recall:  60.00%; FB1:  70.59  14
            value: precision:  36.36%; recall:  40.00%; FB1:  38.10  22
             year: precision:  98.08%; recall:  98.08%; FB1:  98.08  104
```

The evaluation report include the two label levels from MAPA.
The TOKEN-WISE evaluation means that each token in compared individually to account for 
right/wrong predictions.
The CoNLL-wise evaluation reuse the usual CoNLL evaluation criteria of comparing full (IOB-tagged) 
entities. That means that for an entity to be considered correct, all the tokens belonging to
it (i.e. the whole span of the entity) must be correct.

