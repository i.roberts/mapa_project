## MAPA entity replacement

Once the entities have been detected according to the corresponding Deep Learning model and 
regular expression module, MAPA allows replacing their text spans by different means.

### Simple obfuscation

One is a simple obfuscation. The target entities are replaced by asterisks (*) so the original 
sensitive information cannot be read. This is the most simple approach, but has obvious limitations.
First, it hinders a natural read and understanding of the document and potentially deters the 
obfuscated document from being used for any additional NLP-related process. Also, since all the 
sensitive entities are ideally meant to be replaced by asterisks, when an entity is not correctly
detected and replaced, the information leak is obvious to the eye.

### Neural Language Model based Replacement

In this case the replacement is done using a neural language model.
A neural language model is a language model trained using a neural network. Modern neural language
models are based on Transformers, which are state-of-the-art capable of learning powerful language
models, even in a multilingual setting.

MAPA allows plugging a BERT-based pre-trained models to fulfill this purpose. There are several 
pre-trained BERT models shared by the community, including the BERT multilingual shared by Google.

For each detected entity, the model predicts a list of suitable replacement candidates. The 
suitability of the candidates is calculated based on the probability assigned by the language model
according to the context and the replaced word itself. Then, among the top candidates, one is randomly
sampled.

This approach relies on how good the language model is for a given language, and has also some 
limitations. Sometimes the replacements, despite making sense for the given context, are not what
intuitively they should be, and it is difficult to control certain situations.
However, the replacement happens, and most of the time it is reasonably correct, leading to natural and
readable texts. The use of a language model implies that certain context information might be used to
perform coherent replacements, e.g. the gender of the name, or replacing a city name of a certain 
country by another city name from the same country, etc.
Even most relevant is that, since the entities are replaced by other similar entities, if at some
point an entity is leaked, it is almost impossible to tell whether it was the original entity or a 
replacement.

A real example (executed by the code of this repository) would be:

```text
The MAPA project kick-off meeting took place in Valencia in January 2020.
```

Using the neural LM replacement results into:

```text
The PLAN project kick-off meeting took place in Paris in September 2014.
```

### Randomized replacer

For entities that may follow certain patterns, such ID numbers or phone numbers, there is also a
random replacer. It randomly replaces each character by another character of the same type: a letter by
a letter, and a number by a number.

```text
Mr. Smith made a call from 965434345
```
```text
Mr. George made a call from 696741394
```

### Gazetteer replacer

In addition, MAPA allows using name lists (gazetteers) to make the replacements.
The name lists are stored in simple text files, one name per line.
The names must be for a certain type of entity (e.g. male/female given names, family names, etc.).
Each file can be configured for a certain entity type, so the words detected as being of one of
these entity types will be replaced by randomly sampled names from the corresponding file.