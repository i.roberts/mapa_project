### MAPA configuration file

In order to launch the MAPA service/demo, the MAPA configuration file must be filled.
All the assets (models, regex files, gazetteer files, etc.) must be placed on the same base folder.

The base folder is set using the MAPA_BASE_FOLDER environment variable (when using Docker, it must 
point to the container internal folder in which the actual content from the host is being mounted).

The configuration file is in YAML format.
It means that the indentations are meaningful since they indicate the nesting of the sections.
The file is quite long because there are a lot of things to configure.

Following, there is an example of configuration file.
Each section contains some self-explanatory comments.

```yaml
####################################
# MAPA anonymization configuration
####################################

# if a model needs to be downloaded (e.g. HF base models for replacement) it will be cached there (it is a HF built-in mechanism)
cache_folder: /DATA/agarciap_data/python_stuff/pytorch-torchtext-data/CACHE
##############################
# DETECTION MODELS to be loaded, comment out the ones you don't want to load
##############################
detection_models:
  eurlex_all:
    model_name: eurlex_ALL_v3_epoch124_iter109375_L1_miF-0.8374_L1_binF-0.8669_L2_miF-0.8467_loss-6.2376
    cuda_device: -1
  legal_es:
    model_name: ES_LEGAL+25+eurlexES_e2FL_v1_epoch53_iter3078_L1_miF-0.9551_L1_binF-0.9831_L2_miF-0.9532_loss-1.0528
    cuda_device: -1
###############################
# REPLACEMENT MODELS to be loaded, comment out the ones you don't want to load
###############################
replacement_models:
  bert_multi:
    model_name: bert-base-multilingual-cased
    cuda_device: -1
  ixambert:
    model_name: ixa-ehu/ixambert-base-cased
    cuda_device: -1
  polish_bert:
    model_name: dkleczek/bert-base-polish-cased-v1
    cuda_device: -1

#################################################
# STACKS of DETECTION + REPLACEMENT models, with a readable label that represents them
# Pointers to loaded model labels, comment out the stacks you want to omit
#################################################
default_anonymization_stack: eurlex_all+bert_multi
anonymization_stacks:
  eurlex_all+bert_multi:
    detection_model: eurlex_all
    replacement_model: bert_multi
  # legal_es+bert_multi:
    # detection_model: legal_es
    # replacement_model: bert_multi
  legal_es+ixambert:
    detection_model: legal_es
    replacement_model: ixambert
  eurlex_all+polish_bert:
    detection_model: eurlex_all
    replacement_model: polish_bert



#####################
# OPERATIONAL CONFIGURATION THAT CONTROLS THE ANALYSIS
#####################

#########################
# DEMO/SHOWCASE related config
#########################
showcase:
  default_text_file: default_input_text.txt

##########################
# DETECTION related config
##########################
detection:
  sequence_len: 300
  context_len: 100
  batch_size: 4
  pattern_matching:
    enabled: true
    regex_files:
      - named_patterns.txt

############################
# REPLACEMENT related config
############################
replacement:
  # simple obfuscation (replacement by '*' symbols)
  obfuscation:
    entities_to_obfuscate:
      - given name
      - family name
      - ADDRESS
      - AGE
  # neural replacement related configuration
  neural_replacer:
    batch_size: 8
    top_candidates: 10
    chunk_window_size: 30
    # languages for which only the head part of the word will be replaced (maybe suitable for morphologically rich languages, not tested)
    only_head_replacement_langs: []
    # entity types that will be replaced (take into account the two-levels of MAPA entities)
    replaceable_entity_types:
      - PERSON
      # - given name - male
      # - given name - female
      # - family name
      # - AGE
      - ADDRESS
      # - city
      # - month
      # - year
      # - day
      - DATE
      - TIME
      - ORGANISATION
      - AMOUNT
      - VEHICLE
    # as a general heuristic only entity-words starting by capital letter are replaced, except for entity types that appear in this list
    lowercase_replaceable_entity_types:
      - month
    # stopwords will not be eligible as replacement candidates
    stopwords: ['el', 'la', 'de', 'a', 'del', 'le', 'los','las']
    # reserved words will not be eligible as replaceable
    reserved_words: ['DNI', 'Don', 'D', 'Doña', 'Dña','de','los']
    # set a fixed random seed to make the sampling deterministic (suitable for experimentation)
    random_seed: -1
    # Combine the word-to-word similarity with the contextual-lm-probability
    use_non_contextual_sim_reweighting: True
    # The relevance of the contextual-lm-probability in the combination, i.e. contextual * alpha + non_contextual * (1 - alpha)
    contextual_reweighting_alpha: 0.5
    # Automatic Mixed Precision, no need to touch this
    use_amp: True
  # random character replacement (for entities such as identifiers)
  random_replacer:
    # target entity types for this kind of replacement
    replaceable_entity_types:
      - DNI
      - PHONE
      - IBAN
      - EMAIL
    # words/tokens that will not be replaced even if they are matched as part of an entity
    reserved_words: ['DNI']
  # dictionary based replacement, to pick names from predefined lists (not yet implemented, just a placeholder)
  gazetteer_replacer:
    enabled: true
    particles_to_omit: ['del']
    # language-based configuration (with a default fallback), picking names from specific files given the entity type
    default_config:
      given name - male: en_male_names.txt
      given name - female: en_female_names.txt
      family name: en_family_names.txt
    language_config:
      es:
        given name - male: es_male_names.txt
        given name - female: es_female_names.txt
        family name: es_family_names.txt
     

```

As it can be observed, the config file is split in sections.
Each section controls certain submodule (which models are loaded, neural lm replacement configuration, regex module 
configuration, etc.).

Some configuration parameters are directly specified in the file, like target entities or 
reserved word lists, while others point to files that contain the actual information (regular 
expressions, gazetteer name lists, etc.). You can find small examples of these files 
[HERE](../example_files/example_configuration).
Those files must be in the same base folder. Same applies
to the entity detection models, that are meant to be in the same base folder.

Once the configuration is ready, and the files are in their corresponding place, the service 
can be launched.