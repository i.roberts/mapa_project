from dataclasses import dataclass
from typing import List, Optional

Tokens = List[str]
Labels = List[str]


@dataclass
class MapaEntity:
    id: int
    entity_text: str
    entity_type: str
    offset: int


@dataclass
class MapaResult:
    text: str
    level1_entities: List[MapaEntity]
    level2_entities: List[MapaEntity]
    num_tokens: Optional[int] = None
    analysis_seconds: Optional[float] = None
    using_gpu: Optional[bool] = None
    performing_replacement: Optional[bool] = None