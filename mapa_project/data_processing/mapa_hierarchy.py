"""
Representation of the MAPA entity-types hierarchy
"""
from collections import OrderedDict
from typing import Set, Dict, List, Tuple

# TODO: MAPA hierarchy should be external (a editable json file) I think... but that might cause some additional issues

MAPA_HIERARCHY = {
    'PERSON':
        {'ROLE',
         'title',
         'initial name',
         'given name',
         'given name - male',
         'given name - female',
         'family name',
         'family name - male',
         'family name - female',
         'other:name',
         'unresolved:name',
         'AGE',
         'ETHNIC CATEGORY',
         'NATIONALITY',
         'MARITAL STATUS',
         'PROFESSION',
         'telephone number',
         'email',
         'url',
         'other:contact',
         'unresolved:contact',
         'ID document number',
         'social security number',
         'health insurance number',
         'medical record number',
         'other:id',
         'unresolved:id',
         'FINANCIAL',
         'other:role'},
    'DATE': {'standard abbreviation', 'day', 'day of week', 'month', 'year', 'calendar event', 'other:date', 'unresolved:date'},
    'TIME': {},
    'ADDRESS': {'place', 'building', 'street', 'postcode', 'city', 'country', 'territory', 'other:address', 'unresolved:address'},
    'ORGANISATION': {},
    'AMOUNT': {'value', 'unit', 'other:amount', 'unresolved:amount'},
    # Vehicle only for legal domain? Does it apply for EURLEX data? Only the last level or what? (see the picture, only last level is green)
    'VEHICLE': {'licence plate number', 'colour', 'type', 'model', 'build year', 'other:vehicle', 'unresolved:vehicle'}
}

# Another quick-fix to deal with misspelled labels in the datasets (the current problems comes from ORGANIZATION vs. ORGANISATION)
# The content is (MISSPELLED_TAG,CORRECT_TAG) so we can recover a correct tag from an incorrect one, and go on from there
# This approach probably does not generalize or has drawbacks for some labels, but seems reasonable to solve this issue right now
label_misspellings: Tuple[Tuple[str, str], ...] = (
    ('ORGANIZATION', 'ORGANISATION'),
)


class MapaEntitiesHierarchy:

    def __init__(self, hierarchy_dict: Dict[str, Set[str]], label_misspellings: Tuple[Tuple[str, str], ...] = label_misspellings):
        # cast to list to ensure deterministic order (I think it is not critical, but probably a healthy practice)
        self.__hierarchy_dict: OrderedDict[str, List[str]] = OrderedDict()
        for level1_label in sorted(hierarchy_dict.keys()):
            self.__hierarchy_dict[level1_label] = sorted(hierarchy_dict[level1_label])
        self.__reversed_hierarchy = {v: k for k, values in self.__hierarchy_dict.items() for v in values}
        self.__capitalization_correction_map = {**{k.lower(): k for k in self.__hierarchy_dict.keys()},
                                                **{value.lower(): value for values in list(self.__hierarchy_dict.values()) for value in values}}
        self.label_mispellings_map: Dict[str, str] = {incorrect: correct for incorrect, correct in label_misspellings}

    def get_level1_labels(self, bio: bool, lowercase: bool):
        labels: List[str] = list(self.__hierarchy_dict.keys())
        if lowercase:
            labels = [label.lower() for label in labels]
        if bio:
            labels = [f'B-{label}' for label in labels] + [f'I-{label}' for label in labels]
        return labels

    def get_level2_labels(self, level1_label: str, bio: bool, lowercase: bool, input_is_bio_tagged: bool = False):
        if input_is_bio_tagged:
            # For the cases in which we request level2 tags based on an already BIO-tagged level1_tag
            level1_label = level1_label.replace('B-', '').replace('I-', '')
        labels: List[str] = self.__hierarchy_dict[level1_label]
        if lowercase:
            labels = [label.lower() for label in labels]
        if bio:
            labels = [f'B-{label}' for label in labels] + [f'I-{label}' for label in labels]
        return labels

    def get_all_level2_labels(self, bio: bool, lowercase: bool):
        all_level2_labels: List[str] = sorted(self.__reversed_hierarchy.keys())
        if lowercase:
            all_level2_labels = [label.lower() for label in all_level2_labels]
        if bio:
            all_level2_labels = [f'B-{label}' for label in all_level2_labels] + [f'I-{label}' for label in all_level2_labels]
        return all_level2_labels

    def get_level2_parent(self, level2_label, lowercase: bool, input_is_bio_tagged: bool = False):
        if input_is_bio_tagged:
            # For the cases in which we request level1 tag based on an already BIO-tagged level2_tag
            level2_label = level2_label.replace('B-', '').replace('I-', '')
        level1_label: str = self.__reversed_hierarchy[level2_label]
        if lowercase:
            level1_label = level1_label.lower()
        return level1_label

    def normalize_capitalization(self, label):
        # if the lowercase version matches, the correctly-capitalized version is returned, else returns the very same tag (be it correct or not)
        return self.__capitalization_correction_map.get(label.lower(), label)

    def normalize_misspellings(self, label):
        # if the misspelling is a known one (in the misspellings map), return the corrected label, otherwise return the label itself (correct or not)
        return self.label_mispellings_map.get(label, label)

    def to_json(self):
        return self.__hierarchy_dict

    def to_dict(self):
        # NOTE: the same method as to_json, just keep both to avoid confusions (maybe the to_json is misleading and should be removed)
        return self.__hierarchy_dict

    def to_bio_dict(self):
        bio_dict = {}
        for level1, level2_values in self.__hierarchy_dict.items():
            bio_level2_values = set([f'B-{value}' for value in level2_values] + [f'I-{value}' for value in level2_values])
            bio_dict[f'B-{level1}'] = bio_level2_values
            bio_dict[f'I-{level1}'] = bio_level2_values
        return bio_dict

    @classmethod
    def from_json(cls, json_dict: Dict):
        return MapaEntitiesHierarchy(json_dict)


MAPA_ENTITIES_HIERARCHY = MapaEntitiesHierarchy(hierarchy_dict=MAPA_HIERARCHY)
