"""
The idea is to read the tsv files (at least in a first tentative way) to obtain a more direct representation to ingest (i.e. jsonlines...)

So, we make the following assumptions (based on what we have understood so far, revise this later if necessary):

This is a little example of annotation:
1-11	71-77	Países	*[2]|*[3]	country[2]|ADDRESS[3]
1-12	78-83	Bajos	*[2]|*[3]	country[2]|ADDRESS[3]

The text comes above each block with a #Text= prefix per new line.

We need some structures to store the information, prior to the jsonlines conversion: tsv -> internal_repr -> jsonlines

IMPORTANT NOTE: I was already working, but we have noticed that the level1 and level2 annotations are not distinguished by their order.
The order seems arbitrary some times (maybe the labelling order by the annotator). It seems that we can rely on the capitalization though:
 - CAPITAL LETTERS mean first level
 - lowercase letters mean second level

IMPORTANT NOTE 2: It cannot be, there some categories with both level in uppercase PERSON|ROLE, etc.
Better to have a First Level list (it will be short) to ensure that they go first always... OMG, this is getting more complex...

"""
import os
import re
from collections import Counter
from dataclasses import dataclass
from typing import Optional, List, Tuple

# Some global variables to keep track of how many errors are encountered during the parsing (not a best practice, but for this it does the trick)
from mapa_project.data_processing.mapa_hierarchy import MAPA_ENTITIES_HIERARCHY

PARSING_ERRORS: List[str] = []
LABEL_ERROR_RECOVERY_TYPES = Counter()
INVERTED_LABEL_RECOVERY = Counter()


@dataclass
class CharOffset:
    start: int
    end: int


@dataclass
class LabelAnnotation:
    id: int
    label: str

    @staticmethod
    def empty_annotation():
        return LabelAnnotation(-1, 'O')

    @staticmethod
    def parse(annotation: str):
        if annotation == '_':
            return LabelAnnotation.empty_annotation()
        elif '[' not in annotation:
            return LabelAnnotation(-2, annotation)
        else:
            z = re.match(r'([\w\s:-]*\*?)\[(\d+)]', annotation)
            return LabelAnnotation(id=int(z.group(2)), label=z.group(1))


@dataclass
class InceptionTokenAnnotation:
    sentence_num: int
    token_num: int
    offset: CharOffset
    text: str
    level1_label: Optional[LabelAnnotation] = LabelAnnotation.empty_annotation()
    level2_label: Optional[LabelAnnotation] = LabelAnnotation.empty_annotation()

    @classmethod
    def parse_line(cls, line: str) -> 'InceptionTokenAnnotation':
        # Line example: 1-11	71-77	Países	*[2]|*[3]	country[2]|ADDRESS[3]
        # Or (w/o entity annotation):   1-5	33-42	planteada	_	_
        # NOTE: I may need to get also the entity identifier to merge them all for the BIO tagging, omitting for now

        parts = line.split('\t')
        sentence_num, token_num = [int(x) for x in parts[0].split('-')]
        offset = CharOffset(*[int(x) for x in parts[1].split('-')])
        text = parts[2]
        label1 = LabelAnnotation.parse(parts[4].split('|')[0])
        label2 = LabelAnnotation.parse(parts[4].split('|')[1]) if len(parts[4].split('|')) == 2 else LabelAnnotation.empty_annotation()
        # reorder levels checking the capitalization (uppercase:level1, lowercase:level2)
        # THIS WILL THROW EXCEPTIONS WHEN LABELS ARE WRONG, you would want a try-except block here once you check how it goes...
        try:
            cls.__normalize_capitalization_(label_annotation=label1)
            cls.__correct_misspellings_(label_annotation=label1)
            cls.__normalize_capitalization_(label_annotation=label2)
            cls.__correct_misspellings_(label_annotation=label2)
            level1_label, level2_label = cls.__check_and_arrange_labels(label1=label1, label2=label2)
        except Exception as ex:
            print(f'Warning: Exception parsing labels -> {ex}')
            global PARSING_ERRORS
            PARSING_ERRORS.append(f'{ex}')
            level1_label, level2_label = LabelAnnotation.empty_annotation(), LabelAnnotation.empty_annotation()

        #########
        # DEBUGGING EMAIL THINGs
        # if level1_label.label == 'email' or level2_label.label == 'email':
        #     print(f'EMAIL! {level1_label}|{level2_label}', line)
        ##############

        return InceptionTokenAnnotation(sentence_num=sentence_num, token_num=token_num, offset=offset, text=text, level1_label=level1_label,
                                        level2_label=level2_label)

    @classmethod
    def __check_and_arrange_labels(cls, label1: LabelAnnotation, label2: LabelAnnotation) -> Tuple[LabelAnnotation, LabelAnnotation]:
        """ Check the labels against the hierarchy to guess which one is the LEVEL1 and which one is the LEVEL2, if they are valid """
        if label1 == LabelAnnotation.empty_annotation() and label2 == LabelAnnotation.empty_annotation():
            # not exactly sure if this fits the rest of the logic
            return label1, label2
        if label1 and label1.label in MAPA_ENTITIES_HIERARCHY.get_level1_labels(bio=False, lowercase=False):
            if label2 != LabelAnnotation.empty_annotation():
                if label2.label in MAPA_ENTITIES_HIERARCHY.get_level2_labels(label1.label, bio=False, lowercase=False):
                    return label1, label2
                else:
                    raise Exception(f'Invalid labels combination: {label1}, {label2}')
            else:
                return label1, LabelAnnotation.empty_annotation()
        elif label2 and label2.label in MAPA_ENTITIES_HIERARCHY.get_level1_labels(bio=False, lowercase=False):
            INVERTED_LABEL_RECOVERY.update([f'{label2.label} as label2 (with {label1.label} as label1)'])
            if label1 != LabelAnnotation.empty_annotation():
                if label1.label in MAPA_ENTITIES_HIERARCHY.get_level2_labels(label2.label, bio=False, lowercase=False):
                    return label2, label1
                else:
                    raise Exception(f'Invalid labels combination: {label1}, {label2}')
            else:
                return label2, LabelAnnotation.empty_annotation()
        else:
            # raise Exception(f'Neither label1 nor label2 are part of the hierarchy: {label1}, {label2}')
            label1, label2 = cls.__try_to_recover_from_annotation_errors(label1=label1, label2=label2)
            return label1, label2

    @classmethod
    def __try_to_recover_from_annotation_errors(cls, label1: LabelAnnotation, label2: LabelAnnotation) -> Tuple[LabelAnnotation, LabelAnnotation]:
        """ There are annotation errors that are easy to recover from (missing level1 than can be inferred from the level2) """
        LABEL_ERROR_RECOVERY_TYPES.update([f'{sorted([label1.label, label2.label])}'])
        if (label1.label == LabelAnnotation.empty_annotation() or label1.label == '*') and \
                label2.label in MAPA_ENTITIES_HIERARCHY.get_all_level2_labels(bio=False, lowercase=False):
            # the id is not perfect, but we have no other so we mimic the one from the label2 (for single word annotations should be ok)
            label1 = LabelAnnotation(id=label2.id, label=MAPA_ENTITIES_HIERARCHY.get_level2_parent(label2.label, lowercase=False))
            return label1, label2
        elif (label2 == LabelAnnotation.empty_annotation() or label2.label == '*') and \
                label1.label in MAPA_ENTITIES_HIERARCHY.get_all_level2_labels(bio=False, lowercase=False):
            label2 = LabelAnnotation(id=label1.id, label=MAPA_ENTITIES_HIERARCHY.get_level2_parent(label1.label, lowercase=False))
            return label2, label1
        else:
            raise Exception(f'Neither label1 nor label2 are part of the hierarchy (irrecoverable error) -> {label1}, {label2}')

    @classmethod
    def __normalize_capitalization_(cls, label_annotation: LabelAnnotation):
        """ Some annotations come with a slightly different capitalization, match them and convert to canonical capitalization from the hierarchy """
        label_annotation.label = MAPA_ENTITIES_HIERARCHY.normalize_capitalization(label_annotation.label)

    @classmethod
    def __correct_misspellings_(cls, label_annotation: LabelAnnotation):
        """ Some labels contain misspellings (in the sense that they are not exactly the ones defined in the official MAPA hierarchy) """
        label_annotation.label = MAPA_ENTITIES_HIERARCHY.normalize_misspellings(label_annotation.label)


@dataclass
class InceptionSentenceAnnotation:
    lines: List[str]
    tokens_annotations: List[InceptionTokenAnnotation]


@dataclass
class InceptionDocumentAnnotation:
    doc_name: str
    sentences: List[InceptionSentenceAnnotation]


def is_text(line: str):
    return line.strip().startswith('#Text=')


def is_annotation(line):
    return re.match(r'\d+-\d+\t.+', line.strip())


def parse_inception_file(input_path) -> InceptionDocumentAnnotation:
    with open(input_path, 'r', encoding='utf-8') as f:  # beware of the encoding just in case
        lines = f.readlines()

    annotated_sentences: List[InceptionSentenceAnnotation] = []

    text_lines_for_current_sentence: List[str] = []
    token_annotations_for_current_sentence: List[InceptionTokenAnnotation] = []
    for line_num, line in enumerate(lines):
        line = line.strip()
        if is_text(line):
            text_lines_for_current_sentence.append(line[6:])  # remove the #Text= prefix
        elif is_annotation(line):
            if len(text_lines_for_current_sentence) == 0:
                raise Exception(
                    f'No text lines before an annotation, something went wrong.'
                    f'Line:{line_num}|Line content:{line}|Filename:{os.path.basename(input_path)}')
            else:
                token_annotations_for_current_sentence.append(InceptionTokenAnnotation.parse_line(line))

        elif line.strip() == '':
            # empty line, if there is something in the current stuff lists, dump it and clear
            if len(text_lines_for_current_sentence) > 0 and len(token_annotations_for_current_sentence) > 0:
                annotated_sentence = InceptionSentenceAnnotation(lines=text_lines_for_current_sentence,
                                                                 tokens_annotations=token_annotations_for_current_sentence)
                annotated_sentences.append(annotated_sentence)
                text_lines_for_current_sentence = []
                token_annotations_for_current_sentence = []

    # pick the remainder just in case (this was causing a bug! missing last sentences in some cases!)
    if len(text_lines_for_current_sentence) > 0 and len(token_annotations_for_current_sentence) > 0:
        annotated_sentence = InceptionSentenceAnnotation(lines=text_lines_for_current_sentence,
                                                         tokens_annotations=token_annotations_for_current_sentence)
        annotated_sentences.append(annotated_sentence)

    annotated_document = InceptionDocumentAnnotation(doc_name=os.path.basename(input_path), sentences=annotated_sentences)
    return annotated_document


def parse_folders_with_inception_tsv_files(folders: List[str]) -> List[InceptionDocumentAnnotation]:
    parsed_documents: List[InceptionDocumentAnnotation] = []
    file_count = 0
    for _, folder in enumerate(folders):
        files = [os.path.join(folder, filename) for filename in os.listdir(folder) if filename.endswith('.tsv')]
        for file in files:
            print(f'Parsing file number {file_count + 1}: {file}')
            parsed_document = parse_inception_file(input_path=file)
            parsed_documents.append(parsed_document)
            file_count += 1
    print(f'{len(parsed_documents)} files parsed')
    return parsed_documents
