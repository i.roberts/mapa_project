from dataclasses import dataclass, field
from typing import List, Dict, Any, Optional, TypeVar

from torch import Tensor
from torch.utils.data import Dataset

from mapa_project.entity_detection.common.field_definition import FieldDefinition


Dataset_T = TypeVar('Dataset_T', bound=Dataset)


@dataclass
class ModelStepOutput:
    # the loss to perform backward pass of just to log the result
    loss: Tensor
    prediction_scores: Optional[Dict[str, Tensor]] = field(default_factory=dict)  # for example, without any torch.max nor thresholding
    # the gold labels (note that all of them are tensors, we are speaking about indexes, not decoded labels)
    gold_labels: Dict[str, Tensor] = field(default_factory=dict)

    # Added explicitly to fix the problem with Cython and dataclasses (that eventually will get solved)
    # See: https://stackoverflow.com/questions/56079419/using-dataclasses-with-cython
    __annotations__ = {
        'loss': Tensor,
        'prediction_scores': Optional[Dict[str, Tensor]],
        'gold_labels': Dict[str, Tensor]
    }

    def add_prediction_scores(self, output_field: FieldDefinition, prediction_scores: Tensor):
        """ Just commodity method to ease the insertion. Fluent api. """
        self.prediction_scores[output_field.name] = prediction_scores
        return self

    def add_gold_labels(self, output_field: FieldDefinition, labels: Tensor):
        """ Just commodity method to ease the insertion. Fluent api. """
        self.gold_labels[output_field.name] = labels
        return self


@dataclass
class DatasetsVocabsAndExtraResources:
    data_fields: List[FieldDefinition]
    train_dataset: Dataset_T
    dev_dataset: Dataset_T
    extra_resources: Optional[Dict[str, Any]] = None

    # Added explicitly to fix the problem with Cython and dataclasses (that eventually will get solved)
    # See: https://stackoverflow.com/questions/56079419/using-dataclasses-with-cython
    __annotations__ = {
        'data_fields': List[FieldDefinition],
        'train_dataset': Dataset_T,
        'dev_dataset': Dataset_T,
        'extra_resources': Optional[Dict[str, Any]]
    }
