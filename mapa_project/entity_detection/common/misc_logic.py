import logging
import os
import random
import warnings

import numpy as np
import torch
from sklearn.exceptions import UndefinedMetricWarning
from torch import nn
from torch.cuda.amp import autocast
from torch.nn import Module

from typing import List, Set, ClassVar, Dict

from torch.utils.data import DataLoader
from tqdm import tqdm

from mapa_project.entity_detection.training.training_evaluation import BaseEvaluationMetric

logger: logging.Logger = logging.getLogger(__name__)


def preinstantiation_stuff():
    logger.info(' >> WARNING: Filtering/Ignoring deprecation/future/undefined_metric warnings...')
    # Let's see if this allows us to ignore the annoying Tensorflow Deprecation/Future warnings without any unwanted side effect
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    warnings.filterwarnings("ignore", category=FutureWarning)
    warnings.filterwarnings("ignore", category=UndefinedMetricWarning)

    logger.info(' >> ATTENTION: Setting CUDA_DEVICE_ORDER="PCI_BUS_ID"')
    # If it does not run at the very beginning it has no effect (probably before any cuda context gets created)
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"


def set_all_random_seeds_to(seed: int = 42):
    logger.info(f'Setting all random seeds (Python/Numpy/Pytorch) to: {seed}')
    # random seeds for all the involved modules (pytorch, numpy, and general random)
    # if we were using GPU, additional stuff would be needed here
    # see: https://pytorch.org/docs/stable/notes/randomness.html
    torch.manual_seed(seed)
    np.random.seed(seed)
    random.seed(seed)
    if torch.cuda.is_available():  # let's see if this does not crash...
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
        # also NOTE:
        # 'Deterministic mode can have a performance impact, depending on your model.
        # This means that due to the deterministic nature of the model, the processing speed
        # (i.e. processed batch items per second) can be lower than when the model is non-deterministic.'
    # ####################


def use_cuda(cuda_devices: str):
    return torch.cuda.is_available() and cuda_devices and '-1' not in cuda_devices


def is_multigpu_training(cuda_devices: str):
    return use_cuda(cuda_devices) and len(cuda_devices.split(',')) > 1


##############
# The get_learning_rate goes apart, it is just an utility method, not related to the optimizer instantiation
##############

def get_learning_rate(optimizer):
    """ Not sure if this works or if it is the best way..."""
    for param_group in optimizer.param_groups:
        return param_group['lr']


class AutocastModel(Module):
    """
    A wrapper to apply the auto-cast in every module, so the model developer does not need to apply it manually.
    It seems to work (or at least it does not crash), but I cannot tell whether it is doing anything beneficial yet.
    """

    def __init__(self, module: Module, use_amp: bool):
        super(AutocastModel, self).__init__()
        self.module = module
        self.use_amp = use_amp

    def forward(self, *inputs, **kwargs):
        with autocast(enabled=self.use_amp):
            return self.module(*inputs, **kwargs)

    def __getattr__(self, name):
        try:
            return super().__getattr__(name)
        except AttributeError:
            return getattr(self.module, name)


class ExtendedDataParallel(nn.DataParallel):
    def __getattr__(self, name):
        try:
            return super().__getattr__(name)
        except AttributeError:
            return getattr(self.module, name)


def to_data_parallel(model: Module, cuda_devices: str, use_amp: bool) -> Module:
    """
    Return the model after parallelization and moving to corresponding device.
    Note that this method is not meant to be inherited.
    Also note the dependency of the ExtendedDataParallel (of my own) from the old base training codebase.
    """
    # default device if not overridden by the cuda configuration
    device = torch.device('cpu')
    if torch.cuda.is_available() and use_cuda(cuda_devices):
        # Making the model forward auto-casteable for amp (Automatic Mixed Precision), see:
        # https://pytorch.org/docs/master/notes/amp_examples.html#working-with-multiple-gpus
        model = AutocastModel(model, use_amp=use_amp)
        # set the correct cuda visible devices (using pci order)
        if cuda_devices:
            logger.info(f'Using CUDA device/s with id/s: {cuda_devices}')
            logger.info(f'Device names: {[torch.cuda.get_device_name(int(device_id)) for device_id in cuda_devices.split(",")]}')
            lowest_cuda_device_id = min(cuda_devices.split(','))
            device = torch.device(f'cuda:{lowest_cuda_device_id}')
            model = ExtendedDataParallel(model, device_ids=[int(x) for x in cuda_devices.split(',')])
        else:
            logger.info(f'Using all available cuda devices (no cuda_devices parameter has been set)')
            device = torch.device(f'cuda:{0}')
            model = ExtendedDataParallel(model)
    model.to(device)
    return model


""" Progress bar related things """

# PROGRESS_ASCII = ' 🙁😐😊❤'

PROGRESS_BAR_NCOLS: int = 150


def batch_progress_bar(epoch: int, num_epochs: int, dataloader: DataLoader, global_iteration: int = None):
    """Aux function to clean-up the batch progress bar boilerplate, I expect the parameters to remain constant"""
    description = f'Epoch {epoch}/{num_epochs} progress' if not global_iteration \
        else f'Eval (epoch:{epoch}/{num_epochs}, train_iter:{global_iteration}) progress'
    t = tqdm(dataloader,
             # ascii=PROGRESS_ASCII,
             position=0,
             leave=True,
             desc=description,
             # bar_format="{postfix[0]}",
             postfix='', ncols=PROGRESS_BAR_NCOLS
             )
    return t


def report_to_progress_bar(progress_bar, metric_values, metrics_for_progress_bar: List[BaseEvaluationMetric], current_learning_rate: float):
    progress_bar_metrics_names: Set[str] = {metric.name for metric in metrics_for_progress_bar}
    postfix_str = ''.join(
        [f'; {metric_name}:{metric_result:3.4f}' for metric_name, metric_result in metric_values if metric_name in progress_bar_metrics_names])
    progress_bar.postfix = postfix_str + f'; lr:{current_learning_rate:1.7f}'


class TextCleaner:
    REPLACEMENTS: ClassVar[Dict[str, str]] = {
        '’': '\'',
        '‘': '\'',
        'ʹ': '\'',
        '‑': '-',
        '–': '-',
        '—': '-',
        '…': '...',
        '“': '"',
        '”': '"',
        'ˮ': '"'  # not the same, even they seem so
    }

    def clean_text(self, text: str):
        cleaned_text = []
        for c in text:
            cleaned_text.append(self.REPLACEMENTS.get(c, c))
        return ''.join(cleaned_text)