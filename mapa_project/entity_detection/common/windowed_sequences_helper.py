import itertools
import logging
from math import ceil
from typing import List, Dict, ClassVar, Optional

import torch
from torch import Tensor
from tqdm import tqdm

from mapa_project.entity_detection.common.field_definition import FieldDefinition

logger: logging.Logger = logging.getLogger(__name__)


class WindowedSequencesGenerator:
    INSTANCE_IDX: ClassVar[str] = 'idx'
    INSTANCE_ORDER: ClassVar[str] = 'order'

    def __init__(self, valid_seq_len: int, ctx_len: int, fields_to_process: List[FieldDefinition], add_cls_sep: bool):
        self.valid_seq_len = valid_seq_len
        self.ctx_len = ctx_len
        self.fields_to_process = fields_to_process
        self.add_cls_sep = add_cls_sep

    def transform_sequences_to_windows_with_ctx(self, instances: List[Dict[str, List[str]]]) -> List[Dict[str, List[str]]]:
        converted_instances: List[Dict[str, List[str]]] = []
        logger.info(f'Converting instances to windowed sequences with context...')
        for instance_num, instance in enumerate(tqdm(instances)):
            converted_instances += self.__transform_instance(instance_num, instance)
        return converted_instances

    def __transform_instance(self, instance_num, instance: Dict[str, List[str]]) -> List[Dict[str, List[str]]]:
        resulting_windowed_instances: List[Dict[str, List[str]]] = []
        for field in self.fields_to_process:
            windows_with_ctx = self.__obtain_windows_with_context(instance[field.name], field=field)
            for window_num, window_with_ctx in enumerate(windows_with_ctx):
                # assert len(window_with_ctx) == 2 * self.ctx_len + self.valid_seq_len, \
                #     f'All windowed sequences must result in size 2*ctx+seq={2 * self.ctx_len + self.valid_seq_len}, ' \
                #     f'this one was={len(window_with_ctx)}'
                if self.add_cls_sep:
                    window_with_ctx = self.__add_special_tokens(window_with_ctx, field=field)
                if len(resulting_windowed_instances) == window_num:
                    # adding the first field of a resulting windowed instance
                    resulting_windowed_instances.append({self.INSTANCE_IDX: instance_num,
                                                         self.INSTANCE_ORDER: window_num,
                                                         field.name: window_with_ctx})
                else:
                    # when the windowed instance already exists (from a previous field) and we only add another windowed field
                    resulting_windowed_instances[window_num].update({field.name: window_with_ctx})
        return resulting_windowed_instances

    def __obtain_windows_with_context(self, original_sequence: List[str], field: FieldDefinition) -> List[List[str]]:
        windows: List[List[str]] = []
        for i in range(0, len(original_sequence), self.valid_seq_len):
            windows.append(original_sequence[i:i + self.valid_seq_len])
        windows_with_ctx: List[List[str]] = self.__add_ctx_windows(windows=windows, field=field)
        return windows_with_ctx

    def __add_ctx_windows(self, windows: List[List[str]], field: FieldDefinition) -> List[List[str]]:
        windows_with_ctx = []
        for window_num, window in enumerate(windows):
            prev_ctx = self.__obtain_prev_ctx(window_num=window_num, windows=windows, field=field)
            post_ctx = self.__obtain_post_ctx(window_num=window_num, windows=windows, field=field)
            # when we exhaust the instance, the last window may end up shorter that valid_seq_len, add padding to it (post_ctx will also be padding)
            window += [field.pad_token] * (self.valid_seq_len - len(window))
            window_with_ctx = prev_ctx + window + post_ctx
            assert len(window_with_ctx) == 2 * self.ctx_len + len(window)
            windows_with_ctx.append(window_with_ctx)
        return windows_with_ctx

    def __obtain_prev_ctx(self, window_num, windows, field: FieldDefinition) -> List[str]:
        # calculate how many previous sequence do we need for the context (usually will be just one, but to make this general...)
        # also, take into account that number cannot be greater than the current seq num
        n = min(ceil(self.ctx_len / self.valid_seq_len), window_num)
        prev_ctx: List[str] = list(itertools.chain.from_iterable(windows[window_num - n:window_num]))[-self.ctx_len:]
        # complete with padding if necessary (unlikely to happen, but...)
        prev_ctx = [field.pad_token] * (self.ctx_len - len(prev_ctx)) + prev_ctx
        assert len(prev_ctx) == self.ctx_len
        return prev_ctx

    def __obtain_post_ctx(self, window_num, windows, field: FieldDefinition) -> List[str]:
        # calculate how many post sequences do we need for the context (usually will be just one, but to make this general...)
        # also, take into account that number cannot be greater than the current seq num
        n = min(ceil(self.ctx_len / self.valid_seq_len), len(windows))
        post_ctx = list(itertools.chain.from_iterable(windows[window_num + 1:window_num + n + 1]))[:self.ctx_len]
        # complete with padding if necessary (unlikely to happen, but...)
        post_ctx = post_ctx + [field.pad_token] * (self.ctx_len - len(post_ctx))
        assert len(post_ctx) == self.ctx_len
        return post_ctx

    @classmethod
    def __find_first_non_padding_position(cls, seq: List[str], field: FieldDefinition):
        for i, elem in enumerate(seq):
            if elem != field.pad_token:
                return i

    @classmethod
    def __find_last_non_padding_position(cls, seq: List[str], field: FieldDefinition):
        for i, elem in enumerate(seq[::-1]):
            if elem != field.pad_token:
                return len(seq) - i  # check this...

    def __add_special_tokens(self, window_with_ctx: List[str], field: FieldDefinition):
        ww_ctx = window_with_ctx  # just a rename
        first_npp = self.__find_first_non_padding_position(window_with_ctx, field=field)
        last_npp = self.__find_last_non_padding_position(window_with_ctx, field=field)
        window_with_ctx = ww_ctx[:first_npp] + [field.cls_token] + ww_ctx[first_npp:last_npp] + [field.sep_token] + ww_ctx[last_npp:]
        return window_with_ctx

    #######################
    # Reverse helper methods
    #######################
    # NOTE: maybe these should go in other location, specially if they operate with tensors and have little to do with the rest of the class

    # TODO: too many wild and untested assumptions (including the untested usage of some functions), please check them

    @classmethod
    def rebuild_original_sequences(cls, seq_indices: List[int], windowed_sequences: Tensor,
                                   left_ctx_len: int, right_ctx_len: int, pad_idx: Optional[int]) -> List[List]:
        """ A tensor having a shape of Wx(ctx+seq+ctx) where W is the number of windows, seq_indices indicates the grouping of the sequences """
        seq_split_points = cls.__calculate_seq_split_points(seq_indices)
        rebuilt_sequences: List[List] = []
        current_first = 0
        total_ctx_len = left_ctx_len + right_ctx_len
        for split_point in seq_split_points:
            current_seq_windows_with_ctx = windowed_sequences[current_first:split_point, :]
            current_seq_windows = torch.narrow(current_seq_windows_with_ctx, dim=1, start=left_ctx_len,
                                               length=current_seq_windows_with_ctx.shape[1] - total_ctx_len).contiguous().view(-1)
            if pad_idx:
                # NOTE of warning: the padding removal is dangerous for the case of label fields, that may predict padding at certain mid positions
                # That leads to that very position being removed, causing a mismatch in all the following labels, breaking the remapping
                # The padding removal is not really necessary, because the remapping will simply discard all the padding past the last actual token
                # Anyway, beware of this in case it may create more confusion or pesky bugs
                non_padding_mask = current_seq_windows.ne(pad_idx)
                current_seq_windows = torch.masked_select(current_seq_windows, mask=non_padding_mask)
            rebuilt_sequences.append(current_seq_windows.flatten().tolist())
            current_first = split_point
        return rebuilt_sequences

    @classmethod
    def __calculate_seq_split_points(cls, seq_indices: List[int]):
        current_index = seq_indices[0]
        seq_split_points = []
        for i, seq_idx in enumerate(seq_indices):
            if seq_idx != current_index:
                seq_split_points.append(i)
                current_index = seq_idx
        seq_split_points.append(len(seq_indices))
        return seq_split_points
