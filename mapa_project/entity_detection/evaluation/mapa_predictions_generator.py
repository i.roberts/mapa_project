"""
Code to make use of the inferencer systematically to produce results over a set of documents (with or without gold labels).
"""
import json
import os.path
from typing import List, ClassVar, Tuple

from mapa_project.data_processing.jsonlines_files_generation import convert_tsv_folder_into_individual_jsonlines
from mapa_project.entity_detection.common.entity_detection_model import EnhancedTwoFlatLevelsSequenceLabellingModel
from mapa_project.entity_detection.common.pretokenization_helpers import GeneralPreTokenizer
from mapa_project.entity_detection.evaluation.custom_conll_gen_for_eval import CustomMAPACoNLLFormatter, ModelDescriptors
from mapa_project.entity_detection.inference.entities_detection_inferencer import MAPATwoFlatLevelsInferencer, MapaSeqLabellingOutput

ModelPath = str
TestSetFolder = str


class MapaPredictionsGenerator:
    tokens_f: ClassVar[str] = 'tokens'
    level1_tags_f: ClassVar[str] = 'level1_tags'
    level2_tags_f: ClassVar[str] = 'level2_tags'

    def __init__(self, inferencer: MAPATwoFlatLevelsInferencer, model_descriptors: ModelDescriptors):
        self.inferencer = inferencer
        self.model_descriptors = model_descriptors
        self.custom_conll_formatter = CustomMAPACoNLLFormatter()
        self.pretokenizer = GeneralPreTokenizer(prepend_whitespaces=False, remove_linebreaks=False, add_linebreak_tokens=True)

    def generate_predictions_from_inception_tsv(self, input_folder, output_folder: str):
        json_files_output_folder = os.path.join(output_folder, 'jsonlines')
        os.makedirs(json_files_output_folder, exist_ok=True)
        convert_tsv_folder_into_individual_jsonlines(input_folder=input_folder, output_folder=json_files_output_folder, retokenize_tokens=True)
        self.generate_predictions_from_json(input_folder=json_files_output_folder, output_folder=output_folder)

    def generate_predictions_from_json(self, input_folder: str, output_folder: str):
        """
        Generate predictions for the documents in the given input folder
        :param input_folder: the folder containing the (json-lines) documents
        :return:
        """
        files = [os.path.join(input_folder, file) for file in os.listdir(input_folder) if (file.endswith('.json') or file.endswith('.jsonl'))]
        os.makedirs(output_folder, exist_ok=True)
        print(f'Going to generate predictions over {len(files)} files from: {os.path.abspath(input_folder)}')
        for file in files:
            with open(file, 'r', encoding='utf-8') as f:
                instances = [json.loads(line) for line in f.readlines()]

            all_tokens = []
            all_level1_golds = []
            all_level2_golds = []
            all_level1_preds = []
            all_level2_preds = []
            for instance in instances:
                tokens = instance[self.tokens_f]
                gold_level1 = instance.get(self.level1_tags_f, None)
                gold_level2 = instance.get(self.level2_tags_f, None)

                result: MapaSeqLabellingOutput = self.inferencer.make_inference(pretokenized_input=[tokens])[0]

                pred_level1 = result.level1_tags
                pred_level2 = result.level2_tags

                all_tokens += tokens + ['']
                if gold_level1 and gold_level2:
                    all_level1_golds += gold_level1 + ['']
                    all_level2_golds += gold_level2 + ['']
                all_level1_preds += pred_level1 + ['']
                all_level2_preds += pred_level2 + ['']

            if len(all_level1_golds) == 0:
                all_level1_golds = None
                all_level2_golds = None

            custom_conll_lines = self.custom_conll_formatter.format_document(model_descriptors=self.model_descriptors, tokens=all_tokens,
                                                                             level1_golds=all_level1_golds, level2_golds=all_level2_golds,
                                                                             level1_preds=all_level1_preds, level2_preds=all_level2_preds,
                                                                             token_spans=None)
            self.__dump_predictions(output_folder=output_folder, src_file_name=os.path.basename(file), custom_conll_lines=custom_conll_lines)

    def generate_predictions_from_txt(self, input_folder: str, output_folder: str):
        """
        Generate predictions for the raw text documents in the given input folder
        :param input_folder: the folder containing the (json-lines) documents
        :return:
        """
        files = [os.path.join(file) for file in input_folder if file.endswith('.txt')]
        for file in files:
            with open(file, 'r', encoding='utf-8') as f:
                content = f.read()
            tokens, spans = self.pretokenizer.do_pretokenization(raw_text=content, calculate_offsets=True, as_list_of_tuples=False)
            result: MapaSeqLabellingOutput = self.inferencer.make_inference([tokens])[0]
            # replace line_breaks by empty lines for the custom conll
            tokens = [token if token != GeneralPreTokenizer.LINE_BREAK else '' for token in tokens]
            custom_conll_lines = self.custom_conll_formatter.format_document(model_descriptors=self.model_descriptors, tokens=tokens,
                                                                             level1_golds=None, level2_golds=None,
                                                                             level1_preds=result.level1_tags, level2_preds=result.level2_tags,
                                                                             token_spans=spans)
            self.__dump_predictions(output_folder=output_folder, src_file_name=os.path.basename(file), custom_conll_lines=custom_conll_lines)

    @classmethod
    def __dump_predictions(cls, output_folder, src_file_name: str, custom_conll_lines: List[str]):
        output_path = os.path.join(output_folder, src_file_name + '.mapa.tsv')
        with open(output_path, 'w', encoding='utf-8') as f:
            f.writelines(custom_conll_lines)

    @classmethod
    def systematic_predictions_generation(cls, models_and_test_sets: List[Tuple[ModelDescriptors, ModelPath, TestSetFolder]],
                                          base_output_folder: str, cuda_device_num: int = -1):
        """
        Generate predictions systematically from a list of models and test-sets
        :param models_and_test_sets: tuples of (label, model_path, test_set_(base)_path)
        :param base_output_folder: base output folder, to obtain results
        :param cuda_device_num: the number of GPU to use (-1 means CPU)
        :return:
        """
        # We have several things to figure out
        # Tests sets may be composed of individual files (that we want to keep individually)
        # The test-set must then be the folder that contains the test-files (all the child files within that folder)
        # The output should be equivalent, file by file, within a parent folder named after the run label, all within the base folder
        # OMG, it si already done in the original method above...
        for model_descriptors, model_path, testset in models_and_test_sets:
            # NOTE: the model_class argument handling will change IF we get to have more that one arch (plus the mechanism to deal with them)
            inferencer = MAPATwoFlatLevelsInferencer(model_path=model_path, valid_seq_len=300, ctx_window_len=100, batch_size=5,
                                                     cuda_device_num=cuda_device_num,
                                                     model_class=EnhancedTwoFlatLevelsSequenceLabellingModel, mask_level2=True)
            generator = MapaPredictionsGenerator(inferencer=inferencer, model_descriptors=model_descriptors)
            # Warning: we are assuming that the model_descriptors generate a valid folder name (they should, but is it guaranteed?)
            output_folder = os.path.join(base_output_folder, model_descriptors.to_label(omit_field_name=True))  # omit field names to shorten the name
            # os.makedirs(output_folder, exist_ok=True)
            generator.generate_predictions_from_json(input_folder=testset, output_folder=output_folder)
