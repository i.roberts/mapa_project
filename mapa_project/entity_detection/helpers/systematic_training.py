"""
Logic to launch trainings in batch via external configuration.
Still to be decided the exact logic and policy, but the training details should go in a configuration file.
Then, the process will launch the training one after another in a loop.
"""
import json
from dataclasses import dataclass
from multiprocessing import Lock, Process
from typing import List

import confuse

from mapa_project.entity_detection.training.entity_detection_trainer import MapaEntityDetectionTrainer
from mapa_project.entity_detection.training.entity_detection_trainer_config import MapaEntityDetectionTrainerConfig


@dataclass
class SystematicTrainingConfig:
    # The base attributes, common for all the trainings in a systematic training run
    models_cache_dir: str
    checkpoints_folder: str
    num_epochs: int
    early_stopping_patience: int
    # cuda_devices: str  # this should be treated in an special way, to run several trainings if more than a GPU is configured...
    batch_size: int
    lr: float
    valid_seq_len: int
    ctx_len: int
    warmup_epochs: int
    max_checkpoints_to_keep: int
    labels_to_omit: str
    gradients_accumulation_steps: int
    clip_grad_norm: float
    #####
    # The per-training customizable attributes
    model_name: str
    model_version: int
    model_description: str
    pretrained_model_name_or_path: str
    data_dir: str
    train_set: str
    dev_set: str
    random_seed: int

    # @classmethod
    # def load_config_from_files(cls, base_config_file: str, specific_trainings_config_file: str):
    #     with open(base_config_file, 'r', encoding='utf-8') as f:
    #         base_config_json = json.load(f)
    #     with open(specific_trainings_config_file, 'r', encoding='utf-8') as f:
    #         specific_configs: List = json.load(f)
    #
    #     loaded_training_configs = [SystematicTrainingConfig(**base_config_json, **specific_config) for specific_config in specific_configs]
    #     return loaded_training_configs

    @classmethod
    def load_config_from_yaml(cls, yaml_config_path: str):
        config = confuse.Configuration('MAPASystematicTraining', __name__)
        config.set_file(yaml_config_path)

        base_config = config['base_common_config'].get(dict)
        specific_configs = config['specific_training_configs'].get(dict)
        loaded_training_configs = [SystematicTrainingConfig(**base_config, **specific_config) for specific_config in specific_configs.values()]
        return loaded_training_configs


class GpuAndLockPair:
    def __init__(self, gpu: int, lock: Lock):
        self.gpu = gpu
        self.lock = lock


class SystematicTrainingRunning:

    # def __init__(self, base_config_file: str, specific_trainings_config_file):
    #     self.training_configs: List[SystematicTrainingConfig] = SystematicTrainingConfig.load_config_from_files(
    #         base_config_file=base_config_file,
    #         specific_trainings_config_file=specific_trainings_config_file)

    def __init__(self, yaml_config_file: str):
        self.training_configs: List[SystematicTrainingConfig] = SystematicTrainingConfig.load_config_from_yaml(yaml_config_path=yaml_config_file)

    def run_trainings(self, gpus: List[int]):
        """ The trainings must be scattered around the available configured GPUs """
        gpu_locks: List[GpuAndLockPair] = [GpuAndLockPair(gpu=gpu, lock=Lock()) for gpu in gpus]
        for i, systematic_training_config in enumerate(self.training_configs):
            gpu_lock = gpu_locks[i % len(gpu_locks)]
            print(f'Going to use GPU:{gpu_lock.gpu} for training: {systematic_training_config.model_name}')
            # self.__run_training(systematic_training_config=systematic_training_config, gpu_lock=gpu_lock)
            Process(target=self.__run_training, args=(systematic_training_config, gpu_lock)).start()

    @staticmethod
    def __run_training(systematic_training_config: SystematicTrainingConfig, gpu_lock: GpuAndLockPair):
        gpu, lock = gpu_lock.gpu, gpu_lock.lock
        lock.acquire()
        try:
            training_params = vars(systematic_training_config)
            training_params.update({'cuda_devices': str(gpu)})
            config = MapaEntityDetectionTrainerConfig(**training_params)
            trainer = MapaEntityDetectionTrainer(config=config)
            trainer.train()
        except Exception as ex:
            print(f'SOME ERROR IN {systematic_training_config.model_name} --> {ex}')
        finally:
            lock.release()
