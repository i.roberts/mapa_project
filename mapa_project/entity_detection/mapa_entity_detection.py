import logging
import os.path
from dataclasses import dataclass
from typing import List, Tuple, Optional, Dict

from confuse import Configuration

from mapa_project.entity_detection.pattern_matching import PatternMatcher
from mapa_project.entity_detection.common.entity_detection_model import EnhancedTwoFlatLevelsSequenceLabellingModel
from mapa_project.entity_detection.inference.entities_detection_inferencer import MapaSeqLabellingOutput, MAPATwoFlatLevelsInferencer

logger = logging.getLogger(__name__)


@dataclass
class EntityDetectionResult:
    text: str
    tokens: List[str]
    spans: List[Tuple[int, int]]
    level1_tags: List[str]
    level2_tags: List[str]


@dataclass
class EntityDetectionConfig:
    neural_model_path: str
    valid_seq_len: int
    ctx_window_len: int
    batch_size: int
    cuda_device_num: int

    @classmethod
    def load_from_config(cls, base_folder: str, config: Configuration) -> Dict[str, 'EntityDetectionConfig']:
        detection_models = config['detection_models'].get(dict)
        detection_params = config['detection']
        valid_seq_len = detection_params['sequence_len'].get(int)
        ctx_window_len = detection_params['context_len'].get(int)
        batch_size = detection_params['batch_size'].get(int)
        loaded_configs: Dict[str, 'EntityDetectionConfig'] = {}
        for detection_model_label, detection_model_info in detection_models.items():
            detection_model_name = detection_model_info['model_name']
            cuda_device_num = int(detection_model_info['cuda_device'])
            entity_detection_config = EntityDetectionConfig(neural_model_path=os.path.join(base_folder, detection_model_name),
                                                            valid_seq_len=valid_seq_len,
                                                            ctx_window_len=ctx_window_len,
                                                            batch_size=batch_size,
                                                            cuda_device_num=cuda_device_num)
            loaded_configs[detection_model_label] = entity_detection_config
        return loaded_configs


@dataclass
class PatternMatchingConfig:
    named_patterns_files: Optional[List[str]]
    direct_named_patterns: Optional[List[Tuple[str, str]]]
    pattern_matching_enabled: bool
    pattern_files_hot_reload: bool

    @classmethod
    def load_from_config(cls, base_folder: str, config: Configuration) -> 'PatternMatchingConfig':
        pattern_matching_info = config['detection']['pattern_matching']
        enabled = pattern_matching_info['enabled'].get(bool)
        regex_files = pattern_matching_info['regex_files'].get(list)
        return PatternMatchingConfig(named_patterns_files=[os.path.join(base_folder, re_file) for re_file in regex_files], direct_named_patterns=None,
                                     pattern_matching_enabled=enabled, pattern_files_hot_reload=True)


class MapaEntityDetector:

    def __init__(self, entity_detector_config: EntityDetectionConfig, pattern_matching_config: PatternMatchingConfig):
        """ Needs to instantiate the inferencer, the pattern-matcher, and whatever else """
        self.entity_detector_config = entity_detector_config
        self.nerc_inferencer = MAPATwoFlatLevelsInferencer(model_path=entity_detector_config.neural_model_path,
                                                           valid_seq_len=entity_detector_config.valid_seq_len,
                                                           ctx_window_len=entity_detector_config.ctx_window_len,
                                                           batch_size=entity_detector_config.batch_size,
                                                           cuda_device_num=entity_detector_config.cuda_device_num,
                                                           model_class=EnhancedTwoFlatLevelsSequenceLabellingModel,
                                                           mask_level2=True,
                                                           use_amp=True,
                                                           force_level1_from_level2=True)

        self.pattern_matching_config = pattern_matching_config
        self.pattern_matcher = self.__load_pattern_matcher(pattern_matching_config=pattern_matching_config)
        # if self.pattern_matching_config.pattern_matching_enabled:
        #     if self.pattern_matching_config.named_patterns_files:
        #         self.pattern_matcher = PatternMatcher.load_from_files(self.pattern_matching_config.named_patterns_files)
        #     elif self.pattern_matching_config.direct_named_patterns:
        #         self.pattern_matcher = PatternMatcher(self.pattern_matching_config.direct_named_patterns)
        #     else:
        #         # if nothing is configured empty one
        #         logger.warning('No patterns were configured for pattern matcher despite being enabled')
        #         self.pattern_matcher = PatternMatcher([])

    @classmethod
    def __load_pattern_matcher(cls, pattern_matching_config: PatternMatchingConfig):
        pattern_matcher = PatternMatcher([])
        if pattern_matching_config.pattern_matching_enabled:
            if pattern_matching_config.named_patterns_files:
                pattern_matcher = PatternMatcher.load_from_files(pattern_matching_config.named_patterns_files)
            elif pattern_matching_config.direct_named_patterns:
                pattern_matcher = PatternMatcher(pattern_matching_config.direct_named_patterns)
            else:
                # if nothing is configured empty one
                logger.warning('No patterns were configured for pattern matcher despite being enabled')
                # pattern_matcher = PatternMatcher([])
        return pattern_matcher

    def update_operational_config(self, config: Configuration, base_folder: str):
        """ A method that further propagates (or applies) the configuration changes that can be updated on-the-fly """
        detector_config = list(EntityDetectionConfig.load_from_config(base_folder=base_folder, config=config).values())[0]
        self.nerc_inferencer.batch_size = detector_config.batch_size
        self.nerc_inferencer.valid_seq_len = detector_config.valid_seq_len
        self.nerc_inferencer.ctx_window_len = detector_config.ctx_window_len
        pattern_matching_config = PatternMatchingConfig.load_from_config(base_folder=base_folder, config=config)
        try:
            self.pattern_matcher = self.__load_pattern_matcher(pattern_matching_config)
            self.pattern_matching_config = pattern_matching_config
        except Exception as ex:
            logger.error(f'Some error occurred: {ex}')

    def detect_entities(self, text, tokens: List[str], spans: List[Tuple[int, int]]) -> EntityDetectionResult:
        # note how we are picking the first item, since we feed a single text wrapping it into a list of a single element
        nerc_output: MapaSeqLabellingOutput = self.nerc_inferencer.make_inference([tokens])[0]
        level1_tags, level2_tags = nerc_output.level1_tags, nerc_output.level2_tags
        if self.pattern_matching_config.pattern_matching_enabled:
            labels_with_patterns = self.detect_entities_with_regex(tokens=tokens, spans=spans)
            # update with the added pattern-based labels
            # level1_tags_plus_patterns = self.__merge_labels(label_sequences=[level1_tags, labels_with_patterns])
            level1_tags, level2_tags = self.__merge_ai_and_regex_labels(ai_level1_labels=level1_tags, ai_level2_labels=level2_tags,
                                                                        regex_labels=labels_with_patterns)
        # else:
        #     level1_tags_plus_patterns = level1_tags  # a no-op, simply reassign the variable
        entity_detection_result = EntityDetectionResult(text=text, tokens=tokens, spans=spans, level1_tags=level1_tags,
                                                        level2_tags=level2_tags)
        return entity_detection_result

    def detect_entities_with_ai_model(self, tokens: List[str]) -> MapaSeqLabellingOutput:
        nerc_output: MapaSeqLabellingOutput = self.nerc_inferencer.make_inference([tokens])[0]
        return nerc_output

    def detect_entities_with_regex(self, tokens: List[str], spans: List[Tuple[int, int]]):
        if self.pattern_matching_config.pattern_files_hot_reload:
            self.__hot_reload_patterns()
        labels_with_patterns = self.pattern_matcher.match_entities(tokens=tokens, spans=spans, labels=['O'] * len(tokens))
        return labels_with_patterns

    def __hot_reload_patterns(self):
        if self.pattern_matching_config.named_patterns_files:
            self.pattern_matcher = PatternMatcher.load_from_files(self.pattern_matching_config.named_patterns_files)

    # @classmethod
    # def __merge_labels(cls, label_sequences: List[List[str]], first_wins: bool = False) -> List[str]:
    #     """ Merge the label sequences, giving priority w.r.t the order of the input list (i.e. the first wins or not, depending on the flag) """
    #     # assuming all sequences are of the same length
    #     if not first_wins:
    #         label_sequences.reverse()
    #     merged_labels = ['O'] * len(label_sequences[0])
    #     for label_num in range(len(label_sequences[0])):
    #         for seq_num in range(len(label_sequences)):
    #             if label_sequences[seq_num][label_num] != 'O':
    #                 merged_labels[label_num] = label_sequences[seq_num][label_num]
    #                 break
    #     return merged_labels

    @classmethod
    def __merge_ai_and_regex_labels(cls, ai_level1_labels: List[str], ai_level2_labels: List[str], regex_labels: [str]) \
            -> Tuple[List[str], List[str]]:
        """
        Merge the labels from the two sources, AI model and regex module, into a single consolidated list.
        The regex entities have priority over AI entities.
        :param ai_level1_labels: level1 tags from the AI model
        :param ai_level2_labels: level2 tags from the AI model
        :param regex_labels: tags from regex module (the only tag level for regex)
        :return:
        """
        merged_level1_labels = ['O'] * len(ai_level1_labels)
        merged_level2_labels = ['O'] * len(ai_level2_labels)
        for i, ai_level1 in enumerate(ai_level1_labels):
            ai_level2 = ai_level2_labels[i]
            regex_label = regex_labels[i]
            if regex_label != 'O':
                # if there is a regex label, use it for level1, and set level2 to O
                merged_level1_labels[i] = regex_label
                merged_level2_labels[i] = 'O'
            else:
                # if no regex label, then use the ai labels (regardless they have entities in them or not)
                merged_level1_labels[i] = ai_level1
                merged_level2_labels[i] = ai_level2
        return merged_level1_labels, merged_level2_labels
