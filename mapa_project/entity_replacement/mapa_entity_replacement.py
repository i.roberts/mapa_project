"""
The class that combines all the logic and the potential sources of replacements (neural entity_replacement, random entity_replacement, list-based entity_replacement...)
"""
from dataclasses import dataclass
from typing import Dict, List, Tuple

from confuse import Configuration

from mapa_project.anonymization.datatypes_definition import Tokens, Labels
from mapa_project.entity_replacement.gazetteer_based_replacement import GazetteerReplacerConfig, GazetteerReplacer
from mapa_project.entity_replacement.general_random_replacer import GeneralRandomReplacerConfig, GeneralRandomizedReplacer
from mapa_project.entity_replacement.neural_lm_entity_replacement import NeuralLMEntityReplacerConfig, NeuralLMEntityReplacer
from mapa_project.entity_detection.common.pretokenization_helpers import CharSpan


@dataclass
class EntityReplacementConfig:
    neural_entity_replacer_config: NeuralLMEntityReplacerConfig
    general_random_replacer_config: GeneralRandomReplacerConfig
    gazetteer_replacer_config: GazetteerReplacerConfig

    @classmethod
    def load_from_config(cls, base_folder: str, config: Configuration) -> Dict[str, 'EntityReplacementConfig']:
        neural_replacement_configs: Dict[str, NeuralLMEntityReplacerConfig] = NeuralLMEntityReplacerConfig.load_from_config(config)
        random_replacer_config: GeneralRandomReplacerConfig = GeneralRandomReplacerConfig.load_from_config(config)
        gazetteer_replacer_config: GazetteerReplacerConfig = GazetteerReplacerConfig.load_from_config(base_folder=base_folder, config=config)

        loaded_entity_replacement_configs: Dict[str, 'EntityReplacementConfig'] = {}
        for label, neural_replacer_config in neural_replacement_configs.items():
            loaded_entity_replacement_configs[label] = EntityReplacementConfig(
                neural_entity_replacer_config=neural_replacer_config,
                general_random_replacer_config=random_replacer_config,
                gazetteer_replacer_config=gazetteer_replacer_config
            )
        return loaded_entity_replacement_configs


class MapaEntityReplacer:

    def __init__(self, entity_replacer_config: EntityReplacementConfig):
        self.neural_replacer: NeuralLMEntityReplacer = NeuralLMEntityReplacer(entity_replacer_config.neural_entity_replacer_config)
        self.random_replacer = GeneralRandomizedReplacer(entity_replacer_config.general_random_replacer_config)
        self.gazetteer_replacer = GazetteerReplacer(entity_replacer_config.gazetteer_replacer_config)

    def update_operational_config(self, config: Configuration, base_folder: str):
        """ A method that further propagates (or applies) the configuration changes that can be updated on-the-fly """
        # TODO: continue here (if possible)
        self.neural_replacer.update_operational_config(config)
        self.random_replacer.update_operational_config(config)
        self.gazetteer_replacer.update_operational_config(config=config, base_folder=base_folder)

    def replace_entities(self, tokens: Tokens, spans: List[CharSpan], linebreaks_positions_and_counts: Dict[int, int],
                         level1_labels: Labels, level2_labels: Labels, lang: str) -> Tuple[str, List[CharSpan]]:
        # the replacements are done is cascade, first neural, then random, then gazetteer (is that correct and optimal?)
        replaced_tokens = self.neural_replacer.replace_entities(tokens=tokens, level1_labels=level1_labels, level2_labels=level2_labels, lang=lang)
        replaced_tokens = self.random_replacer.replace_entities(tokens=replaced_tokens, level1_labels=level1_labels, level2_labels=level2_labels)
        replaced_tokens = self.gazetteer_replacer.replace_entities(tokens=replaced_tokens, level1_labels=level1_labels, level2_labels=level2_labels,
                                                                   lang=lang)

        reprocessed_spans = self.__span_updater(old_tokens=tokens, new_tokens=replaced_tokens, original_spans=spans)
        reprocessed_text = self.__rebuild_text(tokens=replaced_tokens, spans=reprocessed_spans,
                                               linebreaks_positions_and_counts=linebreaks_positions_and_counts)

        return reprocessed_text, reprocessed_spans

    @classmethod
    def __span_updater(cls, old_tokens: List[str], new_tokens: List[str], original_spans: List[CharSpan]):
        new_spans: List[CharSpan] = []
        current_total_shift = 0
        for i, old_token in enumerate(old_tokens):
            new_token = new_tokens[i]
            span = original_spans[i]
            new_start = span[0] + current_total_shift
            new_end = new_start + len(new_token)
            new_spans.append((new_start, new_end))
            current_total_shift += len(new_token) - len(old_token)
        return new_spans

    @classmethod
    def __rebuild_text(cls, tokens: List[str], spans: List[CharSpan],
                       linebreaks_positions_and_counts: Dict[int, int]):
        text = []
        last_offset = 0
        for i, token in enumerate(tokens):
            start, end = spans[i]
            if i - 1 in linebreaks_positions_and_counts:
                text.append('\n' * linebreaks_positions_and_counts[i - 1])
                text.append(' ' * (start - last_offset - linebreaks_positions_and_counts[i - 1]))
            else:
                text.append(' ' * (start - last_offset))
            text.append(token)
            last_offset = end
        return ''.join(text)
