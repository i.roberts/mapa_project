import logging
import random
import re
import string
import time
from copy import deepcopy
from dataclasses import dataclass, field
from math import ceil
from typing import List, FrozenSet, Dict, Tuple, Optional

import torch
import unidecode
from confuse import Configuration
from torch import Tensor
from transformers.models.bert import BertForMaskedLM, BertTokenizer

from mapa_project.anonymization.datatypes_definition import Tokens, Labels
from mapa_project.entity_detection.common.misc_logic import AutocastModel

logger = logging.getLogger(__name__)


@dataclass
class NeuralLMEntityReplacerConfig:
    model_name: str
    cache_dir: str
    replaceable_entity_types: List[str]  # w/o BIO tagging
    lowercase_replaceable_entity_types: List[str]  # e.g. months, that might be in lowercase and we still want to replace them
    # potentially_numerical_entity_types: str # no need of this... if it is a number (the token) it can be guessed from the token itself
    stopwords: List[str]
    reserved_words: List[str]
    batch_size: int
    top_candidates: int
    chunk_window_size: int
    only_head_replacement_languages: field(default_factory=list)
    random_seed: Optional[int]
    cuda_device_num: int
    use_non_contextual_sim_reweighting: bool
    contextual_reweighting_alpha: float
    use_amp: bool = True

    @classmethod
    def load_from_config(cls, config: Configuration) -> Dict[str, 'NeuralLMEntityReplacerConfig']:
        # NOTE: as it is now, models for entity_replacement are HF pre-trained models, so they have no path, but their HF name
        # It could be extended later in order to use some sort of in-house trained model, but I don't think this will happen in the short term
        cache_dir = config['cache_folder'].get(str)
        neural_replacer_info = config['replacement']['neural_replacer']
        replacement_models = config['replacement_models'].get(dict)
        loaded_configs: Dict[str, 'NeuralLMEntityReplacerConfig'] = {}
        for replacement_model_label, replacement_model_info in replacement_models.items():
            neural_replacer_config = NeuralLMEntityReplacerConfig(
                model_name=replacement_model_info['model_name'],
                cache_dir=cache_dir,
                replaceable_entity_types=neural_replacer_info['replaceable_entity_types'].get(list),
                lowercase_replaceable_entity_types=neural_replacer_info['lowercase_replaceable_entity_types'].get(list),
                stopwords=neural_replacer_info['stopwords'].get(list),
                reserved_words=neural_replacer_info['reserved_words'].get(list),
                batch_size=neural_replacer_info['batch_size'].get(int),
                top_candidates=neural_replacer_info['top_candidates'].get(int),
                chunk_window_size=neural_replacer_info['chunk_window_size'].get(int),
                only_head_replacement_languages=neural_replacer_info['only_head_replacement_langs'].get(list),
                random_seed=neural_replacer_info['random_seed'].get(int),
                cuda_device_num=replacement_model_info['cuda_device'],
                use_non_contextual_sim_reweighting=neural_replacer_info['use_non_contextual_sim_reweighting'].get(bool),
                contextual_reweighting_alpha=neural_replacer_info['contextual_reweighting_alpha'].get(float),
                use_amp=neural_replacer_info['use_amp'].get(bool)
            )
            loaded_configs[replacement_model_label] = neural_replacer_config
        return loaded_configs


@torch.no_grad()
class NeuralLMEntityReplacer:

    def __init__(self, replacer_config: NeuralLMEntityReplacerConfig):
        self.model: BertForMaskedLM = BertForMaskedLM.from_pretrained(replacer_config.model_name, cache_dir=replacer_config.cache_dir)
        self.tokenizer = BertTokenizer.from_pretrained(replacer_config.model_name, cache_dir=replacer_config.cache_dir, do_lower_case=False)
        self.batch_size = replacer_config.batch_size
        self.top_candidates = replacer_config.top_candidates
        self.random = random.Random(x=replacer_config.random_seed if replacer_config.random_seed else int(time.time()))
        self.chunk_window_size = replacer_config.chunk_window_size
        self.hard_max_len = 500
        self.device = f'cuda:{replacer_config.cuda_device_num}' if torch.cuda.is_available() and replacer_config.cuda_device_num >= 0 else 'cpu'
        self.model.to(self.device)
        self.model = AutocastModel(self.model, use_amp=replacer_config.use_amp and 'cuda' in self.device)
        # words that will be filtered out from candidates
        self.stopwords = set(replacer_config.stopwords)
        # words that will not be eligible for substitution (NOTE that they are lowercase!)
        self.reserved_words = set([word.lower() for word in replacer_config.reserved_words])
        self.substitutable_entity_types: FrozenSet[str] = frozenset(replacer_config.replaceable_entity_types)
        self.lowercase_entity_types: FrozenSet[str] = frozenset(replacer_config.lowercase_replaceable_entity_types)
        # self.dates_and_numbers_entity_types: FrozenSet[str] = frozenset(dates_and_numbers_entity_types)
        self.only_head_replacement_languages = replacer_config.only_head_replacement_languages
        self.use_non_contextual_sim_reweighting = replacer_config.use_non_contextual_sim_reweighting
        self.contextual_reweighting_alpha = replacer_config.contextual_reweighting_alpha

    def update_operational_config(self, config: Configuration):
        """ A method that further propagates (or applies) the configuration changes that can be updated on-the-fly """
        # TODO: continue here (if possible)
        # the loading obtain a map, but we want just one item, all of them have common operational parameters, and those are what we are looking for
        replacer_config = list(NeuralLMEntityReplacerConfig.load_from_config(config).values())[0]
        # now it is a matter of updating only the parameters we want (all except the model or any other heavy-weight stuff)
        self.batch_size = replacer_config.batch_size
        self.top_candidates = replacer_config.top_candidates
        self.random = random.Random(x=replacer_config.random_seed if replacer_config.random_seed else int(time.time()))
        self.chunk_window_size = replacer_config.chunk_window_size
        # words that will be filtered out from candidates
        self.stopwords = set(replacer_config.stopwords)
        # words that will not be eligible for substitution (NOTE that they are lowercase!)
        self.reserved_words = set([word.lower() for word in replacer_config.reserved_words])
        self.substitutable_entity_types: FrozenSet[str] = frozenset(replacer_config.replaceable_entity_types)
        self.lowercase_entity_types: FrozenSet[str] = frozenset(replacer_config.lowercase_replaceable_entity_types)
        # self.dates_and_numbers_entity_types: FrozenSet[str] = frozenset(dates_and_numbers_entity_types)
        self.only_head_replacement_languages = replacer_config.only_head_replacement_languages
        self.use_non_contextual_sim_reweighting = replacer_config.use_non_contextual_sim_reweighting
        self.contextual_reweighting_alpha = replacer_config.contextual_reweighting_alpha

    def replace_entities(self, tokens: Tokens, level1_labels: Labels, level2_labels: Labels, lang: str) -> Tokens:
        # Note that we rely all the process along, on having always "num_substitutable" things (chunks, candidates, etc.)
        substitutable_positions, subs_tokens_level1_labels, subs_tokens_level2_labels = self.__find_substitutable_token_positions(
            tokens,
            level1_labels,
            level2_labels)
        # the weird list-casting is because sometimes tokens were input as Tuples, and there is some concat op below that need lists
        chunks, subpos_in_chunks = self.__create_chunks_for_substitutions(list(tokens).copy(), substitutable_positions)
        replace_full_words = lang not in self.only_head_replacement_languages
        retokenized_masked_chunks, original_token_values, original_token_indices = \
            self.__retokenize_and_mask(chunks, subpos_in_chunks, replace_full_words=replace_full_words)
        # go through the model in a batched fashion, limiting the max batch size the one configured above
        candidates_per_chunk = []
        num_batches = ceil(len(retokenized_masked_chunks) / self.batch_size)
        for batch_num, batch_start in enumerate(range(0, len(retokenized_masked_chunks), self.batch_size)):
            if batch_num % 5 == 0:
                logger.info(f'Processing batch {batch_num}/{num_batches}...')
            batch = retokenized_masked_chunks[batch_start:batch_start + self.batch_size]
            original_token_indices_in_this_batch = original_token_indices[batch_start:batch_start + self.batch_size]
            token_ids_as_tensor, attention_mask, masked_tokens_positions = self.__prepare_model_input(retokenized_and_masked=batch)
            candidates_per_chunk += self.__run_the_model(token_ids_as_tensor, attention_mask, masked_tokens_positions,
                                                         original_token_indices_in_this_batch)

        sampled_candidates = self.__sample_candidates(candidates_per_chunk=candidates_per_chunk, retokenized_chunks=retokenized_masked_chunks,
                                                      original_token_values=original_token_values,
                                                      substitution_level1_labels=subs_tokens_level1_labels,
                                                      substitution_level2_labels=subs_tokens_level2_labels)

        assert len(sampled_candidates) == len(substitutable_positions)
        # a naive way of trying to make coherent substitutions when the same entity is replaced more than once...
        substitution_memory: Dict[str, str] = {}
        substituted_tokens = list(tokens).copy()
        for i, substitutable_position in enumerate(substitutable_positions):
            original_token = substituted_tokens[substitutable_position]
            if original_token not in substitution_memory:
                substituted_tokens[substitutable_position] = sampled_candidates[i].upper() if original_token.isupper() else sampled_candidates[i]
                substitution_memory[original_token] = substituted_tokens[substitutable_position]
            else:
                # reuse the already performed substitution, so each same original token receives the same entity_replacement
                substituted_tokens[substitutable_position] = substitution_memory[original_token]
        return substituted_tokens

    def __find_substitutable_token_positions(self, tokens: Tokens, level1_labels: Labels, level2_labels: Labels) -> Tuple[List[int], Labels, Labels]:
        """ Find the position of the (pre)tokens to be replaced, and their associated label (to choose the entity_replacement policy later) """
        # remove BIO, if there is any special rule, it will be based on the bare type
        level1_labels_wo_bio = self.__remove_bio_tagging(level1_labels)
        level2_labels_wo_bio = self.__remove_bio_tagging(level2_labels)
        substitutable_positions = [i for i, token in enumerate(tokens)
                                   if self.__is_candidate_for_being_substituted(token=token,
                                                                                level1_tag=level1_labels_wo_bio[i],
                                                                                level2_tag=level2_labels_wo_bio[i])]
        # print('>>>>>>>> SUBS', substitutable_positions, [tokens[p] for p in substitutable_positions])
        substitutable_tokens_level1_labels = [level1_labels_wo_bio[i] for i in substitutable_positions]
        substitutable_tokens_level2_labels = [level2_labels_wo_bio[i] for i in substitutable_positions]
        assert len(substitutable_positions) == len(substitutable_tokens_level1_labels) == len(substitutable_tokens_level2_labels)
        return substitutable_positions, substitutable_tokens_level1_labels, substitutable_tokens_level2_labels

    @classmethod
    def __remove_bio_tagging(cls, labels: Labels):
        return [label.replace('B-', '').replace('I-', '') for label in labels]

    def __is_candidate_for_being_substituted(self, token: str, level1_tag: str, level2_tag: str):
        lowercase_token = token.lower()
        is_candidate = lowercase_token not in self.stopwords and lowercase_token not in self.reserved_words and token not in string.punctuation and \
                       (level1_tag in self.substitutable_entity_types or level2_tag in self.substitutable_entity_types) and \
                       (token[0].isupper() or token.isdigit() or
                        (level1_tag in self.lowercase_entity_types or level2_tag in self.lowercase_entity_types))
        # print('token is digit', token.isdigit())
        # print('>>>> is_candidate', token, is_candidate)
        return is_candidate

    def __create_chunks_for_substitutions(self, tokens: Tokens, substitutable_positions: List[int]):
        chunks: List[Tokens] = []
        # the position of each substitution within each chunk
        subs_pos_in_chunks: List[int] = []
        for sub_pos in substitutable_positions:
            start = max(0, sub_pos - self.chunk_window_size)
            end = min(len(tokens), sub_pos + self.chunk_window_size)
            prev_window = tokens[start:sub_pos]
            subs_token = tokens[sub_pos]
            post_window = tokens[sub_pos + 1:end]
            chunks.append(prev_window + [subs_token] + post_window)
            subs_pos_in_chunks.append(len(prev_window))

        chunks = self.__reprocess_chunks_capitalization(chunks=chunks)
        return chunks, subs_pos_in_chunks

    @classmethod
    def __reprocess_chunks_capitalization(cls, chunks: List[Tokens]):
        """ If a chunk contains too many uppercase letters surrounding the word to substitute, the language models do not work well """
        # This could admit more sophisticated strategies, but for now, let's just titlecase anything that comes in all_caps
        # This point would be a good spot for the yet-to-be recapitalization module
        recapitalized_chunks: List[Tokens] = deepcopy(chunks)
        for chunk_num, chunk in enumerate(chunks):
            for token_num, token in enumerate(chunk):
                if token.isupper():
                    recapitalized_chunks[chunk_num][token_num] = token.title()
        return recapitalized_chunks

    def __retokenize_and_mask(self, chunks: List[Tokens], subs_pos_in_chunks: List[int], replace_full_words: bool):
        retokenized_and_masked: List[Tokens] = []
        original_token_values: List[str] = []
        original_token_indices: List[str] = []
        for chunk_num, chunk in enumerate(chunks):
            bert_tokens = [self.tokenizer.cls_token]
            for token_num, token in enumerate(chunk):
                current_bert_tokens = self.tokenizer.tokenize(token)
                if token_num == subs_pos_in_chunks[chunk_num]:
                    # this is the token to be masked
                    if replace_full_words:
                        # logger.info('>>> PROCESSING AS FULL WORD REPLACEMENT')
                        # add a single mask, disregard any following subwords
                        bert_tokens += [self.tokenizer.mask_token]
                        original_token_values.append(token)
                        # NOTE: adding an extra len-based condition, to avoid reweighting towards too-short words that will probably be stopwords
                        if len(current_bert_tokens) == 1 and len(token) > 2:
                            original_token_indices.append(self.tokenizer.convert_tokens_to_ids([current_bert_tokens[0]])[0])
                        else:
                            # no full token in this model, we do not want comparison, use UNK as placeholder? (should not favour any candidate)
                            original_token_indices.append(self.tokenizer.unk_token_id)
                    else:
                        logger.info('>>> PROCESSING AS HEAD-WORD REPLACEMENT (maybe good for suffix-based agglutinative languages)')
                        # mask the head subtoken, and leave the rest (which probably bear declinations)
                        # We could also try the single mask approach and the model might come up with the declined token, but...
                        # That would rely on the vocabulary, plus if there are declined tokens they will have lower scores probably...
                        bert_tokens += [self.tokenizer.mask_token] + current_bert_tokens[1:]
                        original_token_values.append(current_bert_tokens[0])
                        # In this case (only-head-word-entity_replacement) maybe it is ok to check sim with subwords (so the upper UNK stuff does not apply)
                        original_token_indices.append(self.tokenizer.convert_tokens_to_ids([current_bert_tokens[0]])[0])
                else:
                    bert_tokens += current_bert_tokens
            bert_tokens += [self.tokenizer.sep_token]
            retokenized_and_masked.append(bert_tokens)

        return retokenized_and_masked, original_token_values, original_token_indices

    def __prepare_model_input(self, retokenized_and_masked: List[Tokens]):
        """ Add padding/cropping plus attention mask calculation, plus token_ids calculation, turning everything into batched tensors"""
        longest_size = self.__get_size_of_longest(retokenized_and_masked)
        # now assure that all reach this longest using padding (also crop if necessary, but that should never be necessary)
        all_chunks_token_ids: List[List[int]] = []
        masked_tokens_positions: List[int] = []
        for chunk_num, tokens in enumerate(retokenized_and_masked):
            if len(tokens) > longest_size:
                tokens = tokens[:longest_size]
            elif len(tokens) < longest_size:
                tokens += [self.tokenizer.pad_token] * (longest_size - len(tokens))
            all_chunks_token_ids.append(self.tokenizer.convert_tokens_to_ids(tokens))
            masked_tokens_positions.append(all_chunks_token_ids[chunk_num].index(self.tokenizer.mask_token_id))
        token_ids_as_tensor = torch.tensor(all_chunks_token_ids, dtype=torch.long)
        attention_mask = (token_ids_as_tensor != self.tokenizer.pad_token_id).long()

        return token_ids_as_tensor, attention_mask, masked_tokens_positions

    def __run_the_model(self, token_ids_as_tensor, attention_mask, masked_tokens_positions, original_token_indices) -> List[List[str]]:

        # print(f'Model input shape', token_ids_as_tensor.shape)
        # start_time = time.time()
        outputs = self.model(input_ids=token_ids_as_tensor.to(self.device), attention_mask=attention_mask.to(self.device))[0]
        # end_time = time.time()
        # print(f'Model forward time:{end_time - start_time} seconds...')

        indices = torch.tensor(masked_tokens_positions).to(self.device)
        indices = indices.unsqueeze(-1)
        indices = indices.repeat(1, outputs.shape[2])  # repeat the size of the dimension we gather all from (the logits, i.e. last dim here)
        indices = indices.unsqueeze(1)

        gathered_masked_positions = torch.gather(outputs, dim=1, index=indices).squeeze(1)  # should result in a BxN (N==vocab_size)

        # TODO: here is the position in which we have the info about probabilities for each word
        # Re-weight them by their similarity (model embeddings wise) with the original word
        if self.use_non_contextual_sim_reweighting:
            # contextual_scores = torch.softmax(gathered_masked_positions, dim=1)
            # contextual_scores = torch.softmax(gathered_masked_positions, dim=1)
            # contextual_scores = gathered_masked_positions  # avoid softmax, I suspect that it is narrowing the differences too much
            scores = self.__apply_similarity_reweighting(original_token_indices=original_token_indices, contextual_scores=gathered_masked_positions)
        else:
            scores = gathered_masked_positions

        _, sorted_indices = torch.sort(scores, dim=1, descending=True)
        sorted_indices_lists: List[List[int]] = sorted_indices[:, :1000].tolist()  # a list of lists (if everything is correct up to here...)
        candidates_per_chunk: List[List[str]] = []
        for sorted_idx_list in sorted_indices_lists:
            candidates = self.tokenizer.convert_ids_to_tokens(sorted_idx_list[:1000])  # we are dealing with the whole vocab here, a waste
            candidates_per_chunk.append(candidates)

        return candidates_per_chunk

    def __get_size_of_longest(self, lists: List[List]):
        max_size = max([len(a_list) for a_list in lists])
        return min(max_size, self.hard_max_len)

    def __sample_candidates(self, candidates_per_chunk: List[List[str]], retokenized_chunks: List[Tokens],
                            original_token_values: List[str], substitution_level1_labels: Labels, substitution_level2_labels: Labels):
        sampled_candidates: List[str] = []
        for i, candidates in enumerate(candidates_per_chunk):
            retokenized: List[str] = retokenized_chunks[i]
            original_token_value = original_token_values[i]
            substitution_level1_label = substitution_level1_labels[i]
            substitution_level2_label = substitution_level2_labels[i]
            filtered_candidates = self.__filter_candidates(candidates, original_token_value, substitution_level1_label, substitution_level2_label)
            sampled_candidate = self.random.sample(filtered_candidates, 1)[0]
            logger.info(f'Original token: {original_token_value}  filtered_candidates: {filtered_candidates} ')
            # print(f'Original token: {original_token_value}  filtered_candidates: {filtered_candidates} ')
            remerged_candidate = self.__merge_subtokens_if_required(retokenized, sampled_candidate)
            sampled_candidates.append(remerged_candidate)

        return sampled_candidates

    def __filter_candidates(self, all_candidates: List[str], original_token_value: str, level1_label: str, level2_label: str):
        # the filtering policies are not very... curated...
        # if entity_type == 'DATE' or entity_type == 'NUM':
        if self.__is_numeric(original_token_value):
            filtered_candidates = [candidate for candidate in all_candidates if
                                   candidate.lower() != original_token_value.lower() and self.__is_numeric(candidate)]
            # candidate.lower() != original_token_value.lower() and self.__same_token_type(candidate, original_token_value)]
        else:
            filtered_candidates = [candidate for candidate in all_candidates if
                                   self.__is_suitable_candidate(candidate, original_token_value, level1_label, level2_label)]
        # ensure that we do not run out of candidates due to some error (the same token type thing is ill...)
        if len(filtered_candidates) == 0:
            # then give up filtering, all...
            filtered_candidates = all_candidates  # they will end up being the topK, otherwise we would be sampling the whole vocab...
        return filtered_candidates[:self.top_candidates]

    def __is_suitable_candidate(self, candidate: str, original_token_value: str, level1_label: str, level2_label: str):
        # both_upper = candidate[0].isupper() and original_token_value[0].isupper()
        min_len_unless_orig_shorter = len(candidate) > 3 or len(candidate) >= len(original_token_value)

        # both_upper_or_lower_allowed = both_upper or entity_type in self.lowercase_entity_types
        is_suitable = candidate and candidate.lower() not in self.stopwords and \
                      candidate not in string.punctuation and \
                      min_len_unless_orig_shorter and \
                      (candidate[0].isupper() or level1_label in self.lowercase_entity_types or level2_label in self.lowercase_entity_types) and \
                      unidecode.unidecode(candidate).lower() != unidecode.unidecode(original_token_value).lower() and \
                      unidecode.unidecode(candidate).lower() not in unidecode.unidecode(original_token_value).lower()
        return is_suitable

    @classmethod
    def __is_numeric(cls, orig_token):
        return re.match(r'\d+', orig_token) and re.match(r'\d+', orig_token)

    def __merge_subtokens_if_required(self, retokenized, sampled_candidate):
        parts_to_merge = [sampled_candidate]
        masked_position = retokenized.index(self.tokenizer.mask_token)
        for token in retokenized[masked_position + 1:]:
            if token.startswith('##'):
                parts_to_merge.append(token.replace('##', ''))
            else:
                break
        merged_result = ''.join(parts_to_merge)
        return merged_result

    def __apply_similarity_reweighting(self, original_token_indices: List[int], contextual_scores: Tensor):
        """ The new piece of magic, let's see if it works and if it is reasonable is terms of performance and speed """
        start_time = time.time()
        # contextual_scores should be a BxV tensor (B instances, V vocab size)
        # Ensure that contextual scores are normalized as prob distributions (softmax)
        # normalize scores to a 0-1 range
        contextual_scores -= contextual_scores.min(dim=1, keepdim=True)[0]
        contextual_scores /= contextual_scores.max(dim=1, keepdim=True)[0]

        original_token_indices = torch.tensor(original_token_indices).to(self.device)
        # print(f'(unk tok id: {self.tokenizer.unk_token_id}) original tokens indices', original_token_indices)
        # should be a tensor BxH  (H size of embeddings)
        original_tokens_embs = self.model.bert.embeddings.word_embeddings(original_token_indices)

        model_embeddings = self.model.bert.embeddings.word_embeddings.weight

        cos_dis = torch.cdist(model_embeddings, original_tokens_embs, p=2)
        # shape should be VxB, we can transpose it
        cos_dis = cos_dis.T
        # now it should be BxV, et voila
        cos_sim = 1 - cos_dis
        # WARNING: should I normalize the similarity values to be in consonance with the softmaxed contextual probs?
        # let's try to "normalize" using softmax, but we might screw up the distances...
        # cos_sim = torch.softmax(cos_sim, dim=1)
        # Normalizing to 0-1 range
        cos_sim -= cos_sim.min(dim=1, keepdim=True)[0]
        cos_sim /= cos_sim.max(dim=1, keepdim=True)[0]

        # OK, extra stuff, pick the UNK positions (as they have been set above, and nullify, we do not want any boost on them)
        unk_mask = ~torch.eq(original_token_indices.view([original_token_indices.shape[0], 1]), self.tokenizer.unk_token_id).repeat(1,
                                                                                                                                    cos_sim.shape[1])
        # print('unk mask excerpt', unk_mask[:5, :10])
        # print('UNK MASK shape', unk_mask.shape)
        cos_sim = cos_sim * unk_mask
        # we can combine both, let's assume some alpha (in 0-1 range)
        combined = torch.mul(contextual_scores, self.contextual_reweighting_alpha) + torch.mul(cos_sim, 1 - self.contextual_reweighting_alpha)
        end_time = time.time()
        print(f'>>>>>>>>>> REWEIGHT TIME: {end_time - start_time:.4f} seconds')
        return combined
