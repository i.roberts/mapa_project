import logging
from typing import Optional

# from watchdog.events import FileSystemEventHandler

from mapa_project.anonymization.components_loading_and_managing import LoadedComponentsManager
# from mapa_project.webservice.filewatch_manager import FileWatchManager


# class ConfigFileChangeEventHandler(FileSystemEventHandler):
#
#     def on_modified(self, event):
#         super().on_modified(event)
#         load_or_get_components_manager().hot_reload_config()


_components_manager: Optional[LoadedComponentsManager] = None

# _config_file_watch_manager: Optional[FileWatchManager] = None

logger = logging.getLogger(__name__)


def load_or_get_components_manager():
    # load only once
    global _components_manager
    if _components_manager is None:
        logger.info(f'Going to load components...')
        _components_manager = LoadedComponentsManager()
        logger.info(f'Components loaded...')
    return _components_manager


# def load_or_get_config_file_watch_manager():
#     # load only once
#     global _config_file_watch_manager
#     if _config_file_watch_manager is None:
#         logger.info(f'Going to load config file watch manager...')
#         _config_file_watch_manager = FileWatchManager(ConfigFileChangeEventHandler())
#         logger.info(f'Config file watch manager loaded...')
#     return _config_file_watch_manager
