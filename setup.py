from glob import glob

from setuptools import setup

from mapa_project import __version__

setup(
    name='mapa_project',
    version=__version__,
    packages=['mapa_project',
              'mapa_project.anonymization',
              'mapa_project.cli',
              'mapa_project.data_processing',
              'mapa_project.entity_detection', 'mapa_project.entity_detection.common', 'mapa_project.entity_detection.evaluation',
              'mapa_project.entity_detection.helpers', 'mapa_project.entity_detection.inference', 'mapa_project.entity_detection.training',
              'mapa_project.entity_replacement',
              'mapa_project.webservice',
              'mapa_project.webservice.app', 'mapa_project.webservice.static', 'mapa_project.webservice.templates',
              'mapa_project.runnable_scripts'
              ],
    url='https://mapa-project.eu/',
    license='TBD',
    author='Aitor García Pablos, Montse Cuadros',
    author_email='agarciap@vicomtech.org ; mcuadros@vicomtech.org',
    description='MAPA project implementation for automatic document [pseudo] anonymization',
    python_requires='>=3.7',
    install_requires=[
        # 'torch~=1.7.0+cu101', # to be installed externally prior to the package, cuda version must match the target environment
        'pydantic==1.8.2',
        'pydantic-cli==3.2.0',
        'transformers==4.7.0',
        'tensorboard==2.5.0',
        'dataclasses-serialization==1.3.1',
        'scikit-learn==0.24.2',
        'tqdm==4.61.1',
        'sentencepiece==0.1.95',
        'unidecode==1.2.0',
        'langdetect==1.0.9',
        'fastapi==0.65.2',
        'uvicorn==0.14.0',
        'aiofiles==0.7.0',
        'python-multipart==0.0.5',
        'jinja2==3.0.1',
        'confuse==1.5.0',
        'watchdog==2.1.3',
    ],
    data_files=[
        ('mapa_project/webservice/templates', ['mapa_project/webservice/templates/demo_new_layout.html']),
        ('mapa_project/webservice/static/fonts', glob('mapa_project/webservice/static/fonts/*')),
        ('mapa_project/webservice/static/images', glob('mapa_project/webservice/static/images/*')),
        ('mapa_project/webservice/static/js', glob('mapa_project/webservice/static/js/*')),
        ('mapa_project/webservice/static', ['mapa_project/webservice/static/my-styles.css', 'mapa_project/webservice/static/style-vis.css']),
    ],
    entry_points={
        'console_scripts': [
            'mapa_conversion=mapa_project.cli.entrypoints:mapa_tsv_to_json_conversion_main',
            'mapa_train=mapa_project.cli.entrypoints:mapa_entity_detection_training_main',
            'mapa_train_with_cfg=mapa_project.cli.entrypoints:mapa_entity_detection_training_using_config_file_main',
            'mapa_systematic_train=mapa_project.cli.entrypoints:mapa_entity_detection_systematic_train_main',
            'mapa_eval=mapa_project.cli.entrypoints:mapa_entity_detection_evaluation_main',
            'mapa_service=mapa_project.webservice.main:main',
        ]
    }
)
