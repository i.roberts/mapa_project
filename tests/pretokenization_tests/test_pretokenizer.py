from typing import List, Tuple

from mapa_project.entity_detection.common.pretokenization_helpers import GeneralPreTokenizer


def __apply_spans_over_text(text: str, spans: List[Tuple[int, int]]):
    resulting_text = []
    prev_end = 0
    for start, end in spans:
        if start - prev_end > 0:
            resulting_text.append(text[prev_end:start])
        resulting_text.append(text[start:end])

    return ''.join(resulting_text)


def test_pretokenization():
    text = 'This is a test. This is-another-test?'

    pretokenizer = GeneralPreTokenizer(prepend_whitespaces=False, remove_linebreaks=False, add_linebreak_tokens=False)

    tokens, spans = pretokenizer.do_pretokenization(raw_text=text, calculate_offsets=True, as_list_of_tuples=False)

    assert len(tokens) == len(spans)

    assert __apply_spans_over_text(text=text, spans=spans)


def test_pretokenization_with_linebreak_text():
    text = 'This is a test.\nThis is-another-test?\n'

    pretokenizer = GeneralPreTokenizer(prepend_whitespaces=False, remove_linebreaks=True, add_linebreak_tokens=False)

    tokens, spans = pretokenizer.do_pretokenization(raw_text=text, calculate_offsets=True, as_list_of_tuples=False)
    print(tokens)
    assert len(tokens) == len(spans)

    assert __apply_spans_over_text(text=text, spans=spans)


def test_pretokenization_with_linebreak_text_keeping_them():
    text = 'This is a test.\nThis is-another-test?\n'

    pretokenizer = GeneralPreTokenizer(prepend_whitespaces=False, remove_linebreaks=False, add_linebreak_tokens=False)

    tokens, spans = pretokenizer.do_pretokenization(raw_text=text, calculate_offsets=True, as_list_of_tuples=False)
    print(tokens)
    assert len(tokens) == len(spans)

    assert __apply_spans_over_text(text=text, spans=spans)


def test_pretokenization_with_linebreak_text_adding_linebreak_token():
    text = 'This is a test.\n\nThis is-another-test?\n'

    pretokenizer = GeneralPreTokenizer(prepend_whitespaces=False, remove_linebreaks=False, add_linebreak_tokens=True)

    tokens, spans = pretokenizer.do_pretokenization(raw_text=text, calculate_offsets=True, as_list_of_tuples=False)
    print(tokens)
    print(spans)
    assert len(tokens) == len(spans)

    assert __apply_spans_over_text(text=text, spans=spans)


def test_pretokenization_with_linebreak_and_carriage_return():
    text = 'This is a test.\n\r\n\rThis is-another-test?\n\r'

    pretokenizer = GeneralPreTokenizer(prepend_whitespaces=False, remove_linebreaks=False, add_linebreak_tokens=True)

    tokens, spans = pretokenizer.do_pretokenization(raw_text=text, calculate_offsets=True, as_list_of_tuples=False)
    print(tokens)
    print(spans)
    assert len(tokens) == len(spans)

    assert __apply_spans_over_text(text=text, spans=spans)
